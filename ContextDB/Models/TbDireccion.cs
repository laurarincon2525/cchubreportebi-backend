﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class TbDireccion
    {
        public TbDireccion()
        {
            TbSiniestro = new HashSet<TbSiniestro>();
        }

        public int IdDireccion { get; set; }
        public string Calle { get; set; }
        public string CodigoPostal { get; set; }
        public string Colonia { get; set; }
        public string Ciudad { get; set; }
        public string Municipio { get; set; }
        public string Estado { get; set; }
        public string Carretera { get; set; }
        public int? Kilometro { get; set; }
        public string Referencias { get; set; }

        public virtual ICollection<TbSiniestro> TbSiniestro { get; set; }
    }
}
