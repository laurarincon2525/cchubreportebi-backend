﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class TbModeloEquipo
    {
        public int IdModeloEquipo { get; set; }
        public string Descripcion { get; set; }
    }
}
