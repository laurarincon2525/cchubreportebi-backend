﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class TbOperador
    {
        public TbOperador()
        {
            TbOperadorCertificaciones = new HashSet<TbOperadorCertificaciones>();
        }

        public int IdOperador { get; set; }
        public int IdTransportista { get; set; }
        public string Nombre { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string Edad { get; set; }
        public int Experiencia { get; set; }
        public int IdLicencia { get; set; }
        public string AfiliacionImss { get; set; }
        public string VigenciaImss { get; set; }
        public bool Estatus { get; set; }
        public DateTime FechaAlta { get; set; }
        public string Correo { get; set; }

        public virtual TbTransportista IdTransportistaNavigation { get; set; }
        public virtual ICollection<TbOperadorCertificaciones> TbOperadorCertificaciones { get; set; }
    }
}
