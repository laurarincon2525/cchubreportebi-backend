﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class TbEstatusAseguradora
    {
        public int IdEstatusAseguradora { get; set; }
        public string Descripcion { get; set; }
    }
}
