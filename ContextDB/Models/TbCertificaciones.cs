﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class TbCertificaciones
    {
        public TbCertificaciones()
        {
            TbOperadorCertificaciones = new HashSet<TbOperadorCertificaciones>();
            TbPreguntas = new HashSet<TbPreguntas>();
            TbRecursos = new HashSet<TbRecursos>();
        }

        public int Id { get; set; }
        public string Descripcion { get; set; }
        public decimal PorcentajeAprobacion { get; set; }
        public int CantidadPreguntas { get; set; }
        public int Vigencia { get; set; }
        public bool Activa { get; set; }

        public virtual ICollection<TbOperadorCertificaciones> TbOperadorCertificaciones { get; set; }
        public virtual ICollection<TbPreguntas> TbPreguntas { get; set; }
        public virtual ICollection<TbRecursos> TbRecursos { get; set; }
    }
}
