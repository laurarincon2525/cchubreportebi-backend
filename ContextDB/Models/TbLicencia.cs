﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class TbLicencia
    {
        public int IdLicencia { get; set; }
        public string NumeroLicencia { get; set; }
        public string Tipo { get; set; }
        public string Vigencia { get; set; }
        public DateTime FechaExpedicion { get; set; }
        public string Nacionalidad { get; set; }
        public string OtroTipo { get; set; }
    }
}
