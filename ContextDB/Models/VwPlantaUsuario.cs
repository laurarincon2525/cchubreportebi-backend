﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class VwPlantaUsuario
    {
        public string Username { get; set; }
        public int IdPlanta { get; set; }
        public string Descripcion { get; set; }
    }
}
