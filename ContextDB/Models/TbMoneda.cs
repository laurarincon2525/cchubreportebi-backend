﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class TbMoneda
    {
        public int IdMoneda { get; set; }
        public string Descripcion { get; set; }
    }
}
