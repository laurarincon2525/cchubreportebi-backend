﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class TbTipoEquipo
    {
        public int IdTipoEquipo { get; set; }
        public string Descripcion { get; set; }
    }
}
