﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class TbPreguntas
    {
        public TbPreguntas()
        {
            TbRespuestas = new HashSet<TbRespuestas>();
        }

        public int Id { get; set; }
        public int CertificacionId { get; set; }
        public string Descripcion { get; set; }
        public int Complejidad { get; set; }
        public bool? Activa { get; set; }

        public virtual TbCertificaciones Certificacion { get; set; }
        public virtual ICollection<TbRespuestas> TbRespuestas { get; set; }
    }
}
