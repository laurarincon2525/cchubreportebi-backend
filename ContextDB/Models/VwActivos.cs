﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class VwActivos
    {
        public int IdActivo { get; set; }
        public string IdEquipo { get; set; }
        public string IdTipoEquipo { get; set; }
        public string NoEconomico { get; set; }
        public string Marca { get; set; }
        public string Serie { get; set; }
        public string Modelo { get; set; }
        public DateTime? FechaBaja { get; set; }
        public DateTime? FechaAlta { get; set; }
        public int? IdMotivoBaja { get; set; }
        public decimal? Importe { get; set; }
        public string Moneda { get; set; }
        public string Sucursal { get; set; }
        public string Propietario { get; set; }
        public string NoMotor { get; set; }
        public string Fase { get; set; }
        public string EstatusActivo { get; set; }
        public string GrupoTrabajo { get; set; }
        public DateTime? Fecha { get; set; }
    }
}
