﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class TbMensajesCorreo
    {
        public int IdMensajesCorreo { get; set; }
        public int? EstatusAnterior { get; set; }
        public int? EstatusActual { get; set; }
        public bool? ProductoRecuperable { get; set; }
        public bool? ProductoDanado { get; set; }
        public bool? Robo { get; set; }
        public string Mensaje { get; set; }
        public int? IdDestinatario { get; set; }
    }
}
