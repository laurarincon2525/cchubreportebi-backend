﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class VwOperadorCertificado
    {
        public int IdOperador { get; set; }
        public int IdTransportista { get; set; }
        public int CertificacionId { get; set; }
        public bool OperadorActivo { get; set; }
        public string Descripcion { get; set; }
        public int Vigencia { get; set; }
        public bool CertificacionActiva { get; set; }
        public DateTime? FechaFin { get; set; }
        public decimal? Calificacion { get; set; }
        public bool? Aprobado { get; set; }
        public DateTime? FechaVigencia { get; set; }
        public string VigenciaCertificacion { get; set; }
    }
}
