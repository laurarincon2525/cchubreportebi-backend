﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class TbDetalleSiniestro
    {
        public int IdDetalleSiniestro { get; set; }
        public string NombreLineaTransportista { get; set; }
        public string LineaSubcontratada3Pl { get; set; }
        public int? IdTipoTransporte { get; set; }
        public int? IdTipoCarga { get; set; }
        public int? IdTipoServicio { get; set; }
        public string NombreOperador { get; set; }
        public int? AntiguedadEmpresa { get; set; }
        public int? NumUnidad { get; set; }
        public int? EdadOperador { get; set; }
        public bool? Lesionado { get; set; }
        public int? IdTipoLesion { get; set; }
        public bool? Incapacidad { get; set; }
        public bool? Alcohol { get; set; }
        public bool? Drogas { get; set; }
        public bool? Fatalidad { get; set; }
        public int? IdFatalidad { get; set; }
        public string Comentario { get; set; }
    }
}
