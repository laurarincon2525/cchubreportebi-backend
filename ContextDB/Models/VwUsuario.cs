﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class VwUsuario
    {
        public long UserId { get; set; }
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int? IdTransportista { get; set; }
        public string DescTransportista { get; set; }
        public int? IdPlanta { get; set; }
        public string DescPlanta { get; set; }
        public int RoleId { get; set; }
        public string DescRol { get; set; }
        public string Password { get; set; }
        public string SaltCode { get; set; }
    }
}
