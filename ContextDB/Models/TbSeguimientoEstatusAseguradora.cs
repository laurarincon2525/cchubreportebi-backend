﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class TbSeguimientoEstatusAseguradora
    {
        public int IdSeguimientoEstatusAseguradora { get; set; }
        public int? IdSiniestro { get; set; }
        public int? IdSiniestroGlobal { get; set; }
        public int? EstatusAnterior { get; set; }
        public int? EstatusActual { get; set; }
        public int? IdUsuario { get; set; }
        public DateTime? FechaActualizacion { get; set; }
    }
}
