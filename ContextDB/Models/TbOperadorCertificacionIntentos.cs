﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class TbOperadorCertificacionIntentos
    {
        public int Id { get; set; }
        public int IdOperador { get; set; }
        public int IdTransportista { get; set; }
        public int CertificacionId { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime? FechaFin { get; set; }
        public decimal? Calificacion { get; set; }
        public bool Aprobado { get; set; }

        public virtual TbOperadorCertificaciones TbOperadorCertificaciones { get; set; }
    }
}
