﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class TbActivos
    {
        public int IdActivo { get; set; }
        public int? IdEquipo { get; set; }
        public int? IdTipoEquipo { get; set; }
        public string NoEconomico { get; set; }
        public int? Marca { get; set; }
        public string Serie { get; set; }
        public string Modelo { get; set; }
        public DateTime? FechaBaja { get; set; }
        public DateTime? FechaAlta { get; set; }
        public int? IdMotivoBaja { get; set; }
        public decimal? Importe { get; set; }
        public string Moneda { get; set; }
        public int? IdSucursal { get; set; }
        public int? IdPropietario { get; set; }
        public string NoMotor { get; set; }
        public int? IdFase { get; set; }
        public int? IdEstatusActivo { get; set; }
        public int? IdGrupoTrabajo { get; set; }
        public DateTime? Fecha { get; set; }
    }
}
