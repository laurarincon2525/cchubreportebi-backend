﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class TbCausaRaiz
    {
        public int IdCausaRaiz { get; set; }
        public string Descripcion { get; set; }
    }
}
