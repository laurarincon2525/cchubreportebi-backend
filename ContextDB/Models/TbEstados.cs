﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class TbEstados
    {
        public int IdEstados { get; set; }
        public string Descripcion { get; set; }
        public string Region { get; set; }
    }
}
