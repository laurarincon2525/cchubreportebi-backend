﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class VwSiniestro
    {
        public int IdSiniestro { get; set; }
        public string IdSiniestroText { get; set; }
        public int? IdTipoTransportista { get; set; }
        public string DescTipoTranportista { get; set; }
        public DateTime FechaSiniestro { get; set; }
        public string PlantaResponsable { get; set; }
        public int? IdPlantaResponsable { get; set; }
        public string EstatusSiniestroDescripcion { get; set; }
        public int IdEstatusSiniestro { get; set; }
        public string TipoSiniestro { get; set; }
        public string Ruta { get; set; }
        public string EstadoOcurrioSiniestro { get; set; }
        public int? IdTransportista { get; set; }
        public string LineaTransportista { get; set; }
        public string TipoTransporte { get; set; }
        public string TipoCarga { get; set; }
        public decimal? Fo { get; set; }
        public string Pedido1 { get; set; }
        public string Pedido2 { get; set; }
        public string EstatusProducto { get; set; }
        public string Piramide { get; set; }
        public string Origen { get; set; }
        public string Destino { get; set; }
        public int? IdDireccion { get; set; }
        public int? DocAnexoA { get; set; }
        public string LineaSubcontratada3Pl { get; set; }
        public string IdTipoServicio { get; set; }
        public string NombreOperador { get; set; }
        public string AntiguedadOperador { get; set; }
        public string NumUnidad { get; set; }
        public int? EdadOperador { get; set; }
        public bool? Lesionado { get; set; }
        public string TipoLesion { get; set; }
        public bool? Incapacidad { get; set; }
        public bool? Alcohol { get; set; }
        public bool? Drogas { get; set; }
        public bool? Fatalidad { get; set; }
        public string IdFatalidad { get; set; }
        public string Comentario { get; set; }
        public bool? NoAplicaPago { get; set; }
        public string NoNotaCredito { get; set; }
        public decimal? MontoNotaCredito { get; set; }
        public DateTime? FechaNotaCredito { get; set; }
        public decimal? CostoTransferencia { get; set; }
        public decimal? CostoVenta { get; set; }
        public decimal? TipoCambio { get; set; }
        public int? DiasIncapacidad { get; set; }
        public bool? ProductoDrp { get; set; }
        public bool? ProductoRecuperable { get; set; }
        public decimal? MontoRecuperarAse { get; set; }
        public decimal? MontoRecuperadoTrans { get; set; }
        public decimal? MontoEnRecuperacion { get; set; }
        public decimal? MontoNegociado { get; set; }
        public decimal? MontoPendienteRecuperarTrans { get; set; }
        public string EstatusDeAseguradora { get; set; }
        public decimal? TotalRecuperarAse { get; set; }
        public decimal? PendienteDoc { get; set; }
        public decimal? PendAnalisisAse { get; set; }
        public decimal? NoProcede { get; set; }
        public string PiramideDesc { get; set; }
        public string PuntoSiniestro { get; set; }
    }
}
