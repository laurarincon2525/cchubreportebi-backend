﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class TbEstatusActivos
    {
        public int IdEstatusActivos { get; set; }
        public string Descripcion { get; set; }
    }
}
