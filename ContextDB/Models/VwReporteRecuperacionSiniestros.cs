﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class VwReporteRecuperacionSiniestros
    {
        public int IdSiniestro { get; set; }
        public string Tipo { get; set; }
        public int? IdPlantaResponsable { get; set; }
        public DateTime FechaSiniestro { get; set; }
        public int? IdTransportista { get; set; }
        public int? IdTipoSiniestro { get; set; }
        public int IdEstatusSiniestro { get; set; }
        public int? IdTipoTransporte { get; set; }
        public int? IdTipoCarga { get; set; }
    }
}
