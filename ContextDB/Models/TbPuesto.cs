﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class TbPuesto
    {
        public int IdRol { get; set; }
        public string Descripcion { get; set; }
    }
}
