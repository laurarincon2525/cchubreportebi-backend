﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class TbOperadorCertificaciones
    {
        public TbOperadorCertificaciones()
        {
            TbOperadorCertificacionIntentos = new HashSet<TbOperadorCertificacionIntentos>();
        }

        public int IdOperador { get; set; }
        public int IdTransportista { get; set; }
        public int CertificacionId { get; set; }
        public string CodigoAcceso { get; set; }
        public bool Activo { get; set; }

        public virtual TbCertificaciones Certificacion { get; set; }
        public virtual TbOperador Id { get; set; }
        public virtual ICollection<TbOperadorCertificacionIntentos> TbOperadorCertificacionIntentos { get; set; }
    }
}
