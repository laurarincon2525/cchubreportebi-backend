﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class User
    {
        public User()
        {
            TbArchivo = new HashSet<TbArchivo>();
        }

        public long UserId { get; set; }
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
        public string SaltCode { get; set; }
        public int RoleId { get; set; }
        public int? IdTransportista { get; set; }
        public int? IdPlanta { get; set; }
        public string Code { get; set; }
        public DateTime? ExpireCode { get; set; }

        public virtual Role Role { get; set; }
        public virtual ICollection<TbArchivo> TbArchivo { get; set; }
    }
}
