﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class TbTipoArchivoPorSiniestro
    {
        public int Id { get; set; }
        public int IdTipoArchivo { get; set; }
        public int IdTipoSiniestro { get; set; }

        public virtual TbTipoArchivo IdTipoArchivoNavigation { get; set; }
        public virtual TbTipoSiniestro IdTipoSiniestroNavigation { get; set; }
    }
}
