﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class VwTipoArchivoAseguradora
    {
        public int IdTipoArchivo { get; set; }
        public string Descripcion { get; set; }
    }
}
