﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class VwReporteTiempoAtencion
    {
        public int IdSeguimientoEstatusSiniestro { get; set; }
        public int IdSiniestro { get; set; }
        public int? IdPlantaResponsable { get; set; }
        public DateTime FechaSiniestro { get; set; }
        public int? IdTransportista { get; set; }
        public int? IdTipoSiniestro { get; set; }
        public int IdEstatusSiniestro { get; set; }
        public int? IdTipoTransporte { get; set; }
        public int? IdTipoCarga { get; set; }
        public int? EstatusAnterior { get; set; }
        public string DescEstatus { get; set; }
        public int? EstatusActual { get; set; }
        public int? IdUsuario { get; set; }
        public DateTime? FechaActualizacion { get; set; }
        public DateTime? FechaEstatusAnterior { get; set; }
        public int? TiempoMinutos { get; set; }
        public int? TiempoHoras { get; set; }
        public int? TiempoDias { get; set; }
        public int? Orden { get; set; }
    }
}
