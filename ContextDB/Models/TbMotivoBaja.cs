﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class TbMotivoBaja
    {
        public int IdMotivoBaja { get; set; }
        public string Descripcion { get; set; }
    }
}
