﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class TbEdadOperador
    {
        public int IdEdadOperador { get; set; }
        public string Descripcion { get; set; }
    }
}
