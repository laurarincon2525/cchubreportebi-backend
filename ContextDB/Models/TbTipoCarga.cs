﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class TbTipoCarga
    {
        public int IdTipoCarga { get; set; }
        public string Descripcion { get; set; }
    }
}
