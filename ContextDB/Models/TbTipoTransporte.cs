﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class TbTipoTransporte
    {
        public int IdTipoTransporte { get; set; }
        public string Descripcion { get; set; }
    }
}
