﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class TbSiniestroArchivo
    {
        public int Id { get; set; }
        public int IdArchivo { get; set; }
        public int IdSiniestro { get; set; }
        public int IdTipoArchivo { get; set; }

        public virtual TbArchivo IdArchivoNavigation { get; set; }
    }
}
