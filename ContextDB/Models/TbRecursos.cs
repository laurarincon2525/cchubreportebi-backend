﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class TbRecursos
    {
        public int Id { get; set; }
        public int CertificacionId { get; set; }
        public string Descripcion { get; set; }
        public string Nombre { get; set; }
        public bool Activo { get; set; }
        public string Repositorio { get; set; }

        public virtual TbCertificaciones Certificacion { get; set; }
    }
}
