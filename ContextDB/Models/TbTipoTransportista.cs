﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class TbTipoTransportista
    {
        public int IdTipoTransportista { get; set; }
        public string Descripcion { get; set; }
    }
}
