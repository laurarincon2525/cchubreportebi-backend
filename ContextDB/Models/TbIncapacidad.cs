﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class TbIncapacidad
    {
        public int IdIncapacidad { get; set; }
        public string Descripcion { get; set; }
    }
}
