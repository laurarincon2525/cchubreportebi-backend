﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class VwReporteBi
    {
        public int ZtmSinie { get; set; }
        public string _0calday { get; set; }
        public int? _0calmonth2 { get; set; }
        public string ZtmdPlan { get; set; }
        public string ZtmUser { get; set; }
        public string _0apoLocfr { get; set; }
        public string _0region { get; set; }
        public string ZtmCltrp { get; set; }
        public int? ZapCarri { get; set; }
        public string Ztmnacex { get; set; }
        public string Ztmtcarga { get; set; }
        public string Ztmmodser { get; set; }
        public string ZtmTsini { get; set; }
        public string ZtmDanad { get; set; }
        public string ZtmResp { get; set; }
        public string ZtmAnto { get; set; }
        public string ZtmEdop { get; set; }
        public string ZtmLesio { get; set; }
        public string ZtmClasi { get; set; }
        public string ZtmClapi { get; set; }
        public decimal? _0amount { get; set; }
        public string _0currency { get; set; }
        public int ZtmSinre { get; set; }
        public int Zamount { get; set; }
        public decimal? ZtmdMonr { get; set; }
        public long? ZtmCount { get; set; }
        public string ZtmProd { get; set; }
        public string ZtmRestr { get; set; }
        public int ZtmValua { get; set; }
        public decimal? ZtmMonrt { get; set; }
        public int ZtmSiner { get; set; }
        public decimal? ZtmMonr { get; set; }
        public decimal? ZtmMontr { get; set; }
        public decimal? ZtmMonn { get; set; }
        public int ZtmSinen { get; set; }
        public decimal? ZtmPendr { get; set; }
        public int ZtmMeta { get; set; }
        public int ZtmSpgt { get; set; }
        public string ZtmStats { get; set; }
        public string ZtmStatr { get; set; }
        public decimal? ZtmTrecs { get; set; }
        public decimal? ZtmMrecs { get; set; }
        public decimal? ZtmPendd { get; set; }
        public decimal? ZtmPenda { get; set; }
        public decimal? ZtmNopr { get; set; }
        public DateTime FechaSiniestro { get; set; }
    }
}
