﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class TbEstatusSiniestro
    {
        public TbEstatusSiniestro()
        {
            TbSiniestro = new HashSet<TbSiniestro>();
        }

        public int IdEstatusSiniestros { get; set; }
        public string Descripcion { get; set; }
        public bool VisibleTransportista { get; set; }
        public int? Orden { get; set; }
        public bool? VisibleCoordinador { get; set; }

        public virtual ICollection<TbSiniestro> TbSiniestro { get; set; }
    }
}
