﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class TbTipoServicio
    {
        public int IdTipoServicio { get; set; }
        public string Descripcion { get; set; }
    }
}
