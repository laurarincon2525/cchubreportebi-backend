﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class TbSeccionesCorreo
    {
        public int IdSeccionesCorreo { get; set; }
        public bool? ProductoRecuperable { get; set; }
        public bool? ProductoDanado { get; set; }
        public bool? Robo { get; set; }
        public bool? SinRegla { get; set; }
        public string Mensaje { get; set; }
        public bool? Accidente { get; set; }
    }
}
