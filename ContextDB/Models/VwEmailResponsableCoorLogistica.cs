﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class VwEmailResponsableCoorLogistica
    {
        public int RoleId { get; set; }
        public int? IdPlanta { get; set; }
        public string UserName { get; set; }
    }
}
