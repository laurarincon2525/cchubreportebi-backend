﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace ContextDB.Models
{
    public partial class heigismxcchubxpazcuasddatabase01Context : DbContext
    {
        public heigismxcchubxpazcuasddatabase01Context()
        {
        }

        public heigismxcchubxpazcuasddatabase01Context(DbContextOptions<heigismxcchubxpazcuasddatabase01Context> options)
            : base(options)
        {
        }

        public virtual DbSet<Customers> Customers { get; set; }
        public virtual DbSet<Encuestas> Encuestas { get; set; }
        public virtual DbSet<ReporteBitest> ReporteBitest { get; set; }
        public virtual DbSet<RespTempTbSiniestro> RespTempTbSiniestro { get; set; }
        public virtual DbSet<Role> Role { get; set; }
        public virtual DbSet<TbActivos> TbActivos { get; set; }
        public virtual DbSet<TbAntiguedadOperador> TbAntiguedadOperador { get; set; }
        public virtual DbSet<TbArchivo> TbArchivo { get; set; }
        public virtual DbSet<TbCausaRaiz> TbCausaRaiz { get; set; }
        public virtual DbSet<TbCertificaciones> TbCertificaciones { get; set; }
        public virtual DbSet<TbConfiguracionAviso> TbConfiguracionAviso { get; set; }
        public virtual DbSet<TbConfiguraciones> TbConfiguraciones { get; set; }
        public virtual DbSet<TbControlEstatus> TbControlEstatus { get; set; }
        public virtual DbSet<TbDestinatarios> TbDestinatarios { get; set; }
        public virtual DbSet<TbDetalleSiniestro> TbDetalleSiniestro { get; set; }
        public virtual DbSet<TbDireccion> TbDireccion { get; set; }
        public virtual DbSet<TbEdadOperador> TbEdadOperador { get; set; }
        public virtual DbSet<TbEquipo> TbEquipo { get; set; }
        public virtual DbSet<TbEstados> TbEstados { get; set; }
        public virtual DbSet<TbEstatusActivos> TbEstatusActivos { get; set; }
        public virtual DbSet<TbEstatusAseguradora> TbEstatusAseguradora { get; set; }
        public virtual DbSet<TbEstatusProducto> TbEstatusProducto { get; set; }
        public virtual DbSet<TbEstatusSiniestro> TbEstatusSiniestro { get; set; }
        public virtual DbSet<TbFases> TbFases { get; set; }
        public virtual DbSet<TbFatalidad> TbFatalidad { get; set; }
        public virtual DbSet<TbFiltrosSiniestro> TbFiltrosSiniestro { get; set; }
        public virtual DbSet<TbGrupoTrabajo> TbGrupoTrabajo { get; set; }
        public virtual DbSet<TbIncapacidad> TbIncapacidad { get; set; }
        public virtual DbSet<TbLicencia> TbLicencia { get; set; }
        public virtual DbSet<TbLogAcciones> TbLogAcciones { get; set; }
        public virtual DbSet<TbMarcaEquipo> TbMarcaEquipo { get; set; }
        public virtual DbSet<TbMensajesCorreo> TbMensajesCorreo { get; set; }
        public virtual DbSet<TbModeloEquipo> TbModeloEquipo { get; set; }
        public virtual DbSet<TbMoneda> TbMoneda { get; set; }
        public virtual DbSet<TbMotivoBaja> TbMotivoBaja { get; set; }
        public virtual DbSet<TbNotifications> TbNotifications { get; set; }
        public virtual DbSet<TbOperador> TbOperador { get; set; }
        public virtual DbSet<TbOperadorCertificacionIntentos> TbOperadorCertificacionIntentos { get; set; }
        public virtual DbSet<TbOperadorCertificaciones> TbOperadorCertificaciones { get; set; }
        public virtual DbSet<TbPiramideSiniestro> TbPiramideSiniestro { get; set; }
        public virtual DbSet<TbPlanta> TbPlanta { get; set; }
        public virtual DbSet<TbPreguntas> TbPreguntas { get; set; }
        public virtual DbSet<TbPropietario> TbPropietario { get; set; }
        public virtual DbSet<TbPuesto> TbPuesto { get; set; }
        public virtual DbSet<TbRecursos> TbRecursos { get; set; }
        public virtual DbSet<TbRemplazos> TbRemplazos { get; set; }
        public virtual DbSet<TbRespuestas> TbRespuestas { get; set; }
        public virtual DbSet<TbSeccionesCorreo> TbSeccionesCorreo { get; set; }
        public virtual DbSet<TbSeguimientoEstatusAseguradora> TbSeguimientoEstatusAseguradora { get; set; }
        public virtual DbSet<TbSeguimientoEstatusSiniestro> TbSeguimientoEstatusSiniestro { get; set; }
        public virtual DbSet<TbSiniestro> TbSiniestro { get; set; }
        public virtual DbSet<TbSiniestroArchivo> TbSiniestroArchivo { get; set; }
        public virtual DbSet<TbTipoArchivo> TbTipoArchivo { get; set; }
        public virtual DbSet<TbTipoArchivoPorSiniestro> TbTipoArchivoPorSiniestro { get; set; }
        public virtual DbSet<TbTipoCarga> TbTipoCarga { get; set; }
        public virtual DbSet<TbTipoEquipo> TbTipoEquipo { get; set; }
        public virtual DbSet<TbTipoLesion> TbTipoLesion { get; set; }
        public virtual DbSet<TbTipoServicio> TbTipoServicio { get; set; }
        public virtual DbSet<TbTipoSiniestro> TbTipoSiniestro { get; set; }
        public virtual DbSet<TbTipoTransporte> TbTipoTransporte { get; set; }
        public virtual DbSet<TbTipoTransportista> TbTipoTransportista { get; set; }
        public virtual DbSet<TbTransportista> TbTransportista { get; set; }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<VwActivos> VwActivos { get; set; }
        public virtual DbSet<VwConfiguracionAviso> VwConfiguracionAviso { get; set; }
        public virtual DbSet<VwEmailResponsableAdministrativo> VwEmailResponsableAdministrativo { get; set; }
        public virtual DbSet<VwEmailResponsableCalidad> VwEmailResponsableCalidad { get; set; }
        public virtual DbSet<VwEmailResponsableCoorLogistica> VwEmailResponsableCoorLogistica { get; set; }
        public virtual DbSet<VwEmailResponsableCoorPci> VwEmailResponsableCoorPci { get; set; }
        public virtual DbSet<VwEmailResponsableCoorRiesgos> VwEmailResponsableCoorRiesgos { get; set; }
        public virtual DbSet<VwEmailResponsableCoorTrafico> VwEmailResponsableCoorTrafico { get; set; }
        public virtual DbSet<VwEmailResponsableFinanzas> VwEmailResponsableFinanzas { get; set; }
        public virtual DbSet<VwEmailResponsableJefeAlmacen> VwEmailResponsableJefeAlmacen { get; set; }
        public virtual DbSet<VwEmailResponsablePlanta> VwEmailResponsablePlanta { get; set; }
        public virtual DbSet<VwEmailResponsableServicioCliente> VwEmailResponsableServicioCliente { get; set; }
        public virtual DbSet<VwEmailResponsableTransportista> VwEmailResponsableTransportista { get; set; }
        public virtual DbSet<VwOperadorCertificado> VwOperadorCertificado { get; set; }
        public virtual DbSet<VwOperadorLicencia> VwOperadorLicencia { get; set; }
        public virtual DbSet<VwPlantaUsuario> VwPlantaUsuario { get; set; }
        public virtual DbSet<VwReporteBi> VwReporteBi { get; set; }
        public virtual DbSet<VwReporteCausaRaiz> VwReporteCausaRaiz { get; set; }
        public virtual DbSet<VwReporteEstatus> VwReporteEstatus { get; set; }
        public virtual DbSet<VwReporteRecuperacionSin> VwReporteRecuperacionSin { get; set; }
        public virtual DbSet<VwReporteRecuperacionSiniestros> VwReporteRecuperacionSiniestros { get; set; }
        public virtual DbSet<VwReporteTiempoAtencion> VwReporteTiempoAtencion { get; set; }
        public virtual DbSet<VwResumenActivos> VwResumenActivos { get; set; }
        public virtual DbSet<VwSiniestro> VwSiniestro { get; set; }
        public virtual DbSet<VwSiniestroArchivo> VwSiniestroArchivo { get; set; }
        public virtual DbSet<VwSiniestroAseguradora> VwSiniestroAseguradora { get; set; }
        public virtual DbSet<VwTipoArchivoAseguradora> VwTipoArchivoAseguradora { get; set; }
        public virtual DbSet<VwUsuario> VwUsuario { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Server=hei-gis-mxcchubx-p-azcu-ass-dbserver-01.database.windows.net;Initial Catalog= hei-gis-mxcchubx-p-azcu-asd-database-01;Persist Security Info=False;User ID=Safocaadm;Password=Dy0+w5Lp;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=True;Connection Timeout=30;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customers>(entity =>
            {
                entity.ToTable("CUSTOMERS");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Address)
                    .HasColumnName("ADDRESS")
                    .HasMaxLength(25)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Age).HasColumnName("AGE");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("NAME")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Salary)
                    .HasColumnName("SALARY")
                    .HasColumnType("decimal(18, 2)");
            });

            modelBuilder.Entity<Encuestas>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.Pregunta1)
                    .HasColumnName("pregunta1")
                    .HasMaxLength(10)
                    .IsFixedLength();

                entity.Property(e => e.Pregunta2).HasColumnName("pregunta2");
            });

            modelBuilder.Entity<ReporteBitest>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("ReporteBITest");

                entity.Property(e => e.FechaSiniestro).HasColumnType("datetime");

                entity.Property(e => e.Zamount).HasColumnName("ZAMOUNT");

                entity.Property(e => e.ZapCarri).HasColumnName("ZAP_CARRI");

                entity.Property(e => e.ZtmAnto)
                    .HasColumnName("ZTM_ANTO")
                    .HasMaxLength(4000);

                entity.Property(e => e.ZtmClapi)
                    .HasColumnName("ZTM_CLAPI")
                    .HasMaxLength(50);

                entity.Property(e => e.ZtmClasi)
                    .HasColumnName("ZTM_CLASI")
                    .HasMaxLength(19)
                    .IsUnicode(false);

                entity.Property(e => e.ZtmCltrp)
                    .HasColumnName("ZTM_CLTRP")
                    .HasMaxLength(4000);

                entity.Property(e => e.ZtmCount).HasColumnName("ZTM_COUNT");

                entity.Property(e => e.ZtmDanad)
                    .IsRequired()
                    .HasColumnName("ZTM_DANAD")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.ZtmEdop)
                    .HasColumnName("ZTM_EDOP")
                    .HasMaxLength(4000);

                entity.Property(e => e.ZtmLesio)
                    .IsRequired()
                    .HasColumnName("ZTM_LESIO")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.ZtmMeta).HasColumnName("ZTM_META");

                entity.Property(e => e.ZtmMonn)
                    .HasColumnName("ZTM_MONN")
                    .HasColumnType("decimal(22, 4)");

                entity.Property(e => e.ZtmMonr)
                    .HasColumnName("ZTM_MONR")
                    .HasColumnType("decimal(21, 4)");

                entity.Property(e => e.ZtmMonrt)
                    .HasColumnName("ZTM_MONRT")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.ZtmMontr)
                    .HasColumnName("ZTM_MONTR")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.ZtmMrecs)
                    .HasColumnName("ZTM_MRECS")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.ZtmNopr)
                    .HasColumnName("ZTM_NOPR")
                    .HasColumnType("decimal(17, 2)");

                entity.Property(e => e.ZtmPenda)
                    .HasColumnName("ZTM_PENDA")
                    .HasColumnType("decimal(17, 2)");

                entity.Property(e => e.ZtmPendd)
                    .HasColumnName("ZTM_PENDD")
                    .HasColumnType("decimal(17, 2)");

                entity.Property(e => e.ZtmPendr)
                    .HasColumnName("ZTM_PENDR")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.ZtmProd)
                    .IsRequired()
                    .HasColumnName("ZTM_PROD")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.ZtmResp)
                    .IsRequired()
                    .HasColumnName("ZTM_RESP")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.ZtmRestr)
                    .IsRequired()
                    .HasColumnName("ZTM_RESTR")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.ZtmSinen).HasColumnName("ZTM_SINEN");

                entity.Property(e => e.ZtmSiner).HasColumnName("ZTM_SINER");

                entity.Property(e => e.ZtmSinie).HasColumnName("ZTM_SINIE");

                entity.Property(e => e.ZtmSinre).HasColumnName("ZTM_SINRE");

                entity.Property(e => e.ZtmSpgt).HasColumnName("ZTM_SPGT");

                entity.Property(e => e.ZtmStatr)
                    .HasColumnName("ZTM_STATR")
                    .HasMaxLength(50);

                entity.Property(e => e.ZtmStats)
                    .IsRequired()
                    .HasColumnName("ZTM_STATS")
                    .HasMaxLength(14)
                    .IsUnicode(false);

                entity.Property(e => e.ZtmTrecs)
                    .HasColumnName("ZTM_TRECS")
                    .HasColumnType("decimal(17, 2)");

                entity.Property(e => e.ZtmTsini)
                    .HasColumnName("ZTM_TSINI")
                    .HasMaxLength(50);

                entity.Property(e => e.ZtmUser)
                    .HasColumnName("ZTM_USER")
                    .HasMaxLength(50);

                entity.Property(e => e.ZtmValua).HasColumnName("ZTM_VALUA");

                entity.Property(e => e.ZtmYear).HasColumnName("ZTM_YEAR");

                entity.Property(e => e.ZtmdMonr)
                    .HasColumnName("ZTMD_MONR")
                    .HasColumnType("decimal(21, 4)");

                entity.Property(e => e.ZtmdPlan)
                    .HasColumnName("ZTMD_PLAN")
                    .HasMaxLength(4000);

                entity.Property(e => e.Ztmmodser)
                    .HasColumnName("ZTMMODSER")
                    .HasMaxLength(10);

                entity.Property(e => e.Ztmnacex)
                    .HasColumnName("ZTMNACEX")
                    .HasMaxLength(4000);

                entity.Property(e => e.Ztmtcarga)
                    .HasColumnName("ZTMTCARGA")
                    .HasMaxLength(4000);

                entity.Property(e => e._0amount)
                    .HasColumnName("0AMOUNT")
                    .HasColumnType("decimal(18, 2)");

                entity.Property(e => e._0apoLocfr)
                    .HasColumnName("0APO_LOCFR")
                    .HasMaxLength(4000);

                entity.Property(e => e._0calday)
                    .HasColumnName("0CALDAY")
                    .HasMaxLength(4000);

                entity.Property(e => e._0calmonth2).HasColumnName("0CALMONTH2");

                entity.Property(e => e._0currency)
                    .IsRequired()
                    .HasColumnName("0CURRENCY")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e._0region)
                    .HasColumnName("0REGION")
                    .HasMaxLength(3)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<RespTempTbSiniestro>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("resp_temp_tb_Siniestro");

                entity.Property(e => e.CostoTransferencia).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.CostoVenta).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.Destino).HasMaxLength(50);

                entity.Property(e => e.FechaCreacion).HasColumnType("datetime");

                entity.Property(e => e.FechaModificacion).HasColumnType("datetime");

                entity.Property(e => e.FechaNotaCredito).HasColumnType("datetime");

                entity.Property(e => e.FechaSiniestro).HasColumnType("datetime");

                entity.Property(e => e.Fo)
                    .HasColumnName("FO")
                    .HasColumnType("decimal(16, 0)");

                entity.Property(e => e.LineaSubcontratada3Pl).HasColumnName("LineaSubcontratada3PL");

                entity.Property(e => e.MontoNotaCredito).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.MontoPrecioConsumidor).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.MontoRecuperadoTrans).HasColumnType("decimal(10, 0)");

                entity.Property(e => e.NoNotaCredito).HasMaxLength(50);

                entity.Property(e => e.NombreOperador).HasMaxLength(50);

                entity.Property(e => e.NumDolly).HasMaxLength(20);

                entity.Property(e => e.NumRemolque1).HasMaxLength(20);

                entity.Property(e => e.NumRemolque2).HasMaxLength(20);

                entity.Property(e => e.NumTracto).HasMaxLength(20);

                entity.Property(e => e.NumUnidad).HasMaxLength(20);

                entity.Property(e => e.Origen).HasMaxLength(50);

                entity.Property(e => e.Pedido1).HasColumnType("decimal(16, 0)");

                entity.Property(e => e.Pedido2).HasColumnType("decimal(16, 0)");

                entity.Property(e => e.ProductoDrp).HasColumnName("ProductoDRP");

                entity.Property(e => e.PuntoSiniestro).HasMaxLength(100);

                entity.Property(e => e.TipoCambio).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.Username)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TbActivos>(entity =>
            {
                entity.HasKey(e => e.IdActivo);

                entity.ToTable("tb_Activos");

                entity.Property(e => e.Fecha).HasColumnType("datetime");

                entity.Property(e => e.FechaAlta).HasColumnType("date");

                entity.Property(e => e.FechaBaja).HasColumnType("date");

                entity.Property(e => e.Importe).HasColumnType("decimal(10, 0)");

                entity.Property(e => e.Modelo)
                    .HasMaxLength(10)
                    .IsFixedLength();

                entity.Property(e => e.Moneda).HasMaxLength(5);

                entity.Property(e => e.NoEconomico).HasMaxLength(10);

                entity.Property(e => e.NoMotor).HasMaxLength(17);

                entity.Property(e => e.Serie).HasMaxLength(20);
            });

            modelBuilder.Entity<TbAntiguedadOperador>(entity =>
            {
                entity.HasKey(e => e.IdAntiguedadOperador)
                    .HasName("PK_AntiguedadOperador");

                entity.ToTable("tb_AntiguedadOperador");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TbArchivo>(entity =>
            {
                entity.ToTable("tb_Archivo");

                entity.Property(e => e.Descripcion).HasMaxLength(500);

                entity.Property(e => e.Fecha).HasColumnType("datetime");

                entity.Property(e => e.KeyBlob).HasMaxLength(50);

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.RepositorioAzure).HasMaxLength(500);

                entity.HasOne(d => d.IdUsuarioNavigation)
                    .WithMany(p => p.TbArchivo)
                    .HasForeignKey(d => d.IdUsuario)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tb_Archivo_User");
            });

            modelBuilder.Entity<TbCausaRaiz>(entity =>
            {
                entity.HasKey(e => e.IdCausaRaiz);

                entity.ToTable("tb_CausaRaiz");

                entity.Property(e => e.Descripcion).HasMaxLength(50);
            });

            modelBuilder.Entity<TbCertificaciones>(entity =>
            {
                entity.ToTable("tb_Certificaciones");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.PorcentajeAprobacion).HasColumnType("decimal(18, 2)");
            });

            modelBuilder.Entity<TbConfiguracionAviso>(entity =>
            {
                entity.HasKey(e => e.IdConfiguracionAviso)
                    .HasName("PK_ConfiguracionAviso");

                entity.ToTable("tb_ConfiguracionAviso");
            });

            modelBuilder.Entity<TbConfiguraciones>(entity =>
            {
                entity.HasKey(e => e.IdConfiguraciones);

                entity.ToTable("tb_Configuraciones");

                entity.Property(e => e.Configuracion).HasMaxLength(50);
            });

            modelBuilder.Entity<TbControlEstatus>(entity =>
            {
                entity.HasKey(e => e.IdControlEstatus);

                entity.ToTable("tb_ControlEstatus");

                entity.Property(e => e.Fecha).HasColumnType("datetime");
            });

            modelBuilder.Entity<TbDestinatarios>(entity =>
            {
                entity.HasKey(e => new { e.IdDestinatario, e.IdRol });

                entity.ToTable("tb_Destinatarios");
            });

            modelBuilder.Entity<TbDetalleSiniestro>(entity =>
            {
                entity.HasKey(e => e.IdDetalleSiniestro);

                entity.ToTable("tb_DetalleSiniestro");

                entity.Property(e => e.Comentario).HasMaxLength(50);

                entity.Property(e => e.LineaSubcontratada3Pl)
                    .HasColumnName("LineaSubcontratada3PL")
                    .HasMaxLength(10)
                    .IsFixedLength();

                entity.Property(e => e.NombreLineaTransportista).HasMaxLength(50);

                entity.Property(e => e.NombreOperador).HasMaxLength(50);
            });

            modelBuilder.Entity<TbDireccion>(entity =>
            {
                entity.HasKey(e => e.IdDireccion)
                    .HasName("PK__tb_Direc__1F8E0C76C0944AA0");

                entity.ToTable("tb_Direccion");

                entity.Property(e => e.Calle).HasMaxLength(50);

                entity.Property(e => e.Carretera).HasMaxLength(100);

                entity.Property(e => e.Ciudad).HasMaxLength(50);

                entity.Property(e => e.CodigoPostal).HasMaxLength(10);

                entity.Property(e => e.Colonia).HasMaxLength(50);

                entity.Property(e => e.Estado)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Municipio).HasMaxLength(50);

                entity.Property(e => e.Referencias).HasMaxLength(100);
            });

            modelBuilder.Entity<TbEdadOperador>(entity =>
            {
                entity.HasKey(e => e.IdEdadOperador)
                    .HasName("PK_EdadOperador");

                entity.ToTable("tb_EdadOperador");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TbEquipo>(entity =>
            {
                entity.HasKey(e => e.IdEquipo)
                    .HasName("PK_Equipo");

                entity.ToTable("tb_Equipo");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TbEstados>(entity =>
            {
                entity.HasKey(e => e.IdEstados);

                entity.ToTable("tb_Estados");

                entity.Property(e => e.IdEstados).ValueGeneratedNever();

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Region)
                    .HasColumnName("REGION")
                    .HasMaxLength(3)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TbEstatusActivos>(entity =>
            {
                entity.HasKey(e => e.IdEstatusActivos)
                    .HasName("PK_EstatusActivos");

                entity.ToTable("tb_EstatusActivos");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TbEstatusAseguradora>(entity =>
            {
                entity.HasKey(e => e.IdEstatusAseguradora);

                entity.ToTable("tb_EstatusAseguradora");

                entity.Property(e => e.Descripcion).HasMaxLength(50);
            });

            modelBuilder.Entity<TbEstatusProducto>(entity =>
            {
                entity.HasKey(e => e.IdEstatusProducto);

                entity.ToTable("tb_EstatusProducto");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TbEstatusSiniestro>(entity =>
            {
                entity.HasKey(e => e.IdEstatusSiniestros);

                entity.ToTable("tb_EstatusSiniestro");

                entity.Property(e => e.Descripcion).HasMaxLength(50);
            });

            modelBuilder.Entity<TbFases>(entity =>
            {
                entity.HasKey(e => e.IdFases)
                    .HasName("PK_Fases");

                entity.ToTable("tb_Fases");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TbFatalidad>(entity =>
            {
                entity.HasKey(e => e.IdFatalidad);

                entity.ToTable("tb_Fatalidad");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TbFiltrosSiniestro>(entity =>
            {
                entity.HasKey(e => new { e.IdApp, e.IdSiniestro });

                entity.ToTable("tb_FiltrosSiniestro");

                entity.Property(e => e.IdApp)
                    .HasColumnName("idApp")
                    .HasMaxLength(50);

                entity.Property(e => e.AntiguedadOperador).HasMaxLength(50);

                entity.Property(e => e.CostoTransferencia).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.CostoVenta).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.DescTipoTranportista)
                    .HasColumnName("descTipoTranportista")
                    .HasMaxLength(50);

                entity.Property(e => e.Destino).HasMaxLength(50);

                entity.Property(e => e.EstadoOcurrioSiniestro)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.EstatusProducto).HasMaxLength(50);

                entity.Property(e => e.EstatusSiniestroDescripcion)
                    .HasColumnName("EstatusSiniestro_Descripcion")
                    .HasMaxLength(50);

                entity.Property(e => e.FechaNotaCredito).HasColumnType("datetime");

                entity.Property(e => e.FechaSiniestro).HasColumnType("datetime");

                entity.Property(e => e.Fo)
                    .HasColumnName("FO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.IdFatalidad).HasMaxLength(50);

                entity.Property(e => e.IdTipoServicio).HasMaxLength(50);

                entity.Property(e => e.LineaSubcontratada3Pl)
                    .HasColumnName("LineaSubcontratada3PL")
                    .HasMaxLength(50);

                entity.Property(e => e.LineaTransportista).HasMaxLength(50);

                entity.Property(e => e.MontoNotaCredito).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.NoNotaCredito).HasMaxLength(50);

                entity.Property(e => e.NombreOperador).HasMaxLength(50);

                entity.Property(e => e.NumUnidad).HasMaxLength(50);

                entity.Property(e => e.Origen).HasMaxLength(50);

                entity.Property(e => e.Pedido1)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Pedido2)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Piramide).HasMaxLength(50);

                entity.Property(e => e.PlantaResponsable).HasMaxLength(50);

                entity.Property(e => e.ProductoDrp).HasColumnName("ProductoDRP");

                entity.Property(e => e.PuntoSiniestro).HasMaxLength(100);

                entity.Property(e => e.Ruta).HasMaxLength(103);

                entity.Property(e => e.TipoCambio).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.TipoCarga).HasMaxLength(50);

                entity.Property(e => e.TipoLesion).HasMaxLength(50);

                entity.Property(e => e.TipoSiniestro).HasMaxLength(50);

                entity.Property(e => e.TipoTransporte).HasMaxLength(50);
            });

            modelBuilder.Entity<TbGrupoTrabajo>(entity =>
            {
                entity.HasKey(e => e.IdGrupoTrabajo)
                    .HasName("PK_GrupoTrabajo");

                entity.ToTable("tb_GrupoTrabajo");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TbIncapacidad>(entity =>
            {
                entity.HasKey(e => e.IdIncapacidad)
                    .HasName("PK_Incapacidad");

                entity.ToTable("tb_Incapacidad");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TbLicencia>(entity =>
            {
                entity.HasKey(e => e.IdLicencia);

                entity.ToTable("tb_Licencia");

                entity.Property(e => e.FechaExpedicion).HasColumnType("date");

                entity.Property(e => e.Nacionalidad).HasMaxLength(50);

                entity.Property(e => e.NumeroLicencia)
                    .IsRequired()
                    .HasMaxLength(10);

                entity.Property(e => e.OtroTipo).HasMaxLength(50);

                entity.Property(e => e.Tipo)
                    .IsRequired()
                    .HasMaxLength(5);

                entity.Property(e => e.Vigencia)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TbLogAcciones>(entity =>
            {
                entity.ToTable("tb_LogAcciones");

                entity.Property(e => e.Accion)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Fecha).HasColumnType("datetime");

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TbMarcaEquipo>(entity =>
            {
                entity.HasKey(e => e.IdMarcaEquipo)
                    .HasName("PK_MarcaEquipo");

                entity.ToTable("tb_MarcaEquipo");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TbMensajesCorreo>(entity =>
            {
                entity.HasKey(e => e.IdMensajesCorreo);

                entity.ToTable("tb_MensajesCorreo");
            });

            modelBuilder.Entity<TbModeloEquipo>(entity =>
            {
                entity.HasKey(e => e.IdModeloEquipo)
                    .HasName("PK_ModeloEquipo");

                entity.ToTable("tb_ModeloEquipo");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TbMoneda>(entity =>
            {
                entity.HasKey(e => e.IdMoneda)
                    .HasName("PK_Moneda");

                entity.ToTable("tb_Moneda");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TbMotivoBaja>(entity =>
            {
                entity.HasKey(e => e.IdMotivoBaja)
                    .HasName("PK_MotivoBaja");

                entity.ToTable("tb_MotivoBaja");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TbNotifications>(entity =>
            {
                entity.ToTable("tb_Notifications");

                entity.Property(e => e.Active)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.Body)
                    .IsRequired()
                    .HasColumnType("text");

                entity.Property(e => e.Severity)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.TriggerAt)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.UserId)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TbOperador>(entity =>
            {
                entity.HasKey(e => new { e.IdOperador, e.IdTransportista });

                entity.ToTable("tb_Operador");

                entity.Property(e => e.IdOperador).ValueGeneratedOnAdd();

                entity.Property(e => e.AfiliacionImss)
                    .IsRequired()
                    .HasColumnName("AfiliacionIMSS")
                    .HasMaxLength(50);

                entity.Property(e => e.ApellidoMaterno)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.ApellidoPaterno)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Correo).HasMaxLength(100);

                entity.Property(e => e.Edad)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.FechaAlta)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.VigenciaImss)
                    .IsRequired()
                    .HasColumnName("VigenciaIMSS")
                    .HasMaxLength(50);

                entity.HasOne(d => d.IdTransportistaNavigation)
                    .WithMany(p => p.TbOperador)
                    .HasForeignKey(d => d.IdTransportista)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tb_Operador_tb_Transportista");
            });

            modelBuilder.Entity<TbOperadorCertificacionIntentos>(entity =>
            {
                entity.ToTable("tb_OperadorCertificacionIntentos");

                entity.Property(e => e.Calificacion).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.FechaFin).HasColumnType("datetime");

                entity.Property(e => e.FechaInicio).HasColumnType("datetime");

                entity.HasOne(d => d.TbOperadorCertificaciones)
                    .WithMany(p => p.TbOperadorCertificacionIntentos)
                    .HasForeignKey(d => new { d.IdOperador, d.IdTransportista, d.CertificacionId })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tb_OperadorCetificacionIntentos_tb_OperadorCertificaciones");
            });

            modelBuilder.Entity<TbOperadorCertificaciones>(entity =>
            {
                entity.HasKey(e => new { e.IdOperador, e.IdTransportista, e.CertificacionId })
                    .HasName("PK_tb_OperadorCertifications");

                entity.ToTable("tb_OperadorCertificaciones");

                entity.Property(e => e.CodigoAcceso)
                    .IsRequired()
                    .HasMaxLength(36);

                entity.HasOne(d => d.Certificacion)
                    .WithMany(p => p.TbOperadorCertificaciones)
                    .HasForeignKey(d => d.CertificacionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tb_OperadorCertifications_tb_Certifications");

                entity.HasOne(d => d.Id)
                    .WithMany(p => p.TbOperadorCertificaciones)
                    .HasForeignKey(d => new { d.IdOperador, d.IdTransportista })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tb_OperadorCertifications_tb_Operador");
            });

            modelBuilder.Entity<TbPiramideSiniestro>(entity =>
            {
                entity.HasKey(e => e.IdPiramideSiniestro);

                entity.ToTable("tb_PiramideSiniestro");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TbPlanta>(entity =>
            {
                entity.HasKey(e => e.IdPlanta);

                entity.ToTable("tb_Planta");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.ResponsableCet)
                    .HasColumnName("ResponsableCET")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TbPreguntas>(entity =>
            {
                entity.ToTable("tb_Preguntas");

                entity.Property(e => e.Activa)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.Complejidad).HasDefaultValueSql("((1))");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.HasOne(d => d.Certificacion)
                    .WithMany(p => p.TbPreguntas)
                    .HasForeignKey(d => d.CertificacionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tb_Questions_tb_Certifications");
            });

            modelBuilder.Entity<TbPropietario>(entity =>
            {
                entity.HasKey(e => e.IdPropietario)
                    .HasName("PK_Propietario");

                entity.ToTable("tb_Propietario");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TbPuesto>(entity =>
            {
                entity.HasKey(e => e.IdRol)
                    .HasName("PK_tb_Rol");

                entity.ToTable("tb_Puesto");

                entity.Property(e => e.IdRol).ValueGeneratedNever();

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TbRecursos>(entity =>
            {
                entity.ToTable("tb_Recursos");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Repositorio)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.HasOne(d => d.Certificacion)
                    .WithMany(p => p.TbRecursos)
                    .HasForeignKey(d => d.CertificacionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tb_Resources_tb_Certifications");
            });

            modelBuilder.Entity<TbRemplazos>(entity =>
            {
                entity.HasKey(e => e.IdRemplazos)
                    .HasName("PK_Remplazos");

                entity.ToTable("tb_Remplazos");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TbRespuestas>(entity =>
            {
                entity.ToTable("tb_Respuestas");

                entity.Property(e => e.Activa)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.HasOne(d => d.Pregunta)
                    .WithMany(p => p.TbRespuestas)
                    .HasForeignKey(d => d.PreguntaId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tb_Answers_tb_Questions");
            });

            modelBuilder.Entity<TbSeccionesCorreo>(entity =>
            {
                entity.HasKey(e => e.IdSeccionesCorreo);

                entity.ToTable("tb_SeccionesCorreo");

                entity.Property(e => e.IdSeccionesCorreo).HasColumnName("idSeccionesCorreo");
            });

            modelBuilder.Entity<TbSeguimientoEstatusAseguradora>(entity =>
            {
                entity.HasKey(e => e.IdSeguimientoEstatusAseguradora);

                entity.ToTable("tb_SeguimientoEstatusAseguradora");

                entity.Property(e => e.FechaActualizacion).HasColumnType("datetime");
            });

            modelBuilder.Entity<TbSeguimientoEstatusSiniestro>(entity =>
            {
                entity.HasKey(e => e.IdSeguimientoEstatusSiniestro);

                entity.ToTable("tb_SeguimientoEstatusSiniestro");

                entity.Property(e => e.FechaActualizacion).HasColumnType("datetime");

                entity.Property(e => e.FechaEstatusAnterior).HasColumnType("datetime");
            });

            modelBuilder.Entity<TbSiniestro>(entity =>
            {
                entity.HasKey(e => new { e.IdSiniestro, e.FechaSiniestro });

                entity.ToTable("tb_Siniestro");

                entity.Property(e => e.FechaSiniestro).HasColumnType("datetime");

                entity.Property(e => e.CostoTransferencia).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.CostoVenta).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.Destino).HasMaxLength(50);

                entity.Property(e => e.FechaCreacion).HasColumnType("datetime");

                entity.Property(e => e.FechaModificacion).HasColumnType("datetime");

                entity.Property(e => e.FechaNotaCredito).HasColumnType("datetime");

                entity.Property(e => e.Fo)
                    .HasColumnName("FO")
                    .HasColumnType("decimal(16, 0)");

                entity.Property(e => e.LineaSubcontratada3Pl).HasColumnName("LineaSubcontratada3PL");

                entity.Property(e => e.MontoNotaCredito).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.MontoPrecioConsumidor).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.MontoRecuperadoTrans).HasColumnType("decimal(10, 0)");

                entity.Property(e => e.NoAplicaPago).HasDefaultValueSql("((0))");

                entity.Property(e => e.NoNotaCredito).HasMaxLength(50);

                entity.Property(e => e.NombreOperador).HasMaxLength(50);

                entity.Property(e => e.NumDolly).HasMaxLength(20);

                entity.Property(e => e.NumRemolque1).HasMaxLength(20);

                entity.Property(e => e.NumRemolque2).HasMaxLength(20);

                entity.Property(e => e.NumTracto).HasMaxLength(20);

                entity.Property(e => e.NumUnidad).HasMaxLength(20);

                entity.Property(e => e.Origen).HasMaxLength(50);

                entity.Property(e => e.Pedido1).HasColumnType("decimal(16, 0)");

                entity.Property(e => e.Pedido2).HasColumnType("decimal(16, 0)");

                entity.Property(e => e.ProductoDrp).HasColumnName("ProductoDRP");

                entity.Property(e => e.PuntoSiniestro).HasMaxLength(100);

                entity.Property(e => e.TipoCambio).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.Username)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdDireccionNavigation)
                    .WithMany(p => p.TbSiniestro)
                    .HasForeignKey(d => d.IdDireccion)
                    .HasConstraintName("FK_tb_Siniestro_tb_Direccion");

                entity.HasOne(d => d.IdEstatusSiniestroNavigation)
                    .WithMany(p => p.TbSiniestro)
                    .HasForeignKey(d => d.IdEstatusSiniestro)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tb_Siniestro_tb_EstatusSiniestro");
            });

            modelBuilder.Entity<TbSiniestroArchivo>(entity =>
            {
                entity.ToTable("tb_SiniestroArchivo");

                entity.HasOne(d => d.IdArchivoNavigation)
                    .WithMany(p => p.TbSiniestroArchivo)
                    .HasForeignKey(d => d.IdArchivo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tb_SiniestroArchivo_tb_Archivo");
            });

            modelBuilder.Entity<TbTipoArchivo>(entity =>
            {
                entity.HasKey(e => e.IdTipoArchivo)
                    .HasName("PK_TipoArchivo");

                entity.ToTable("tb_TipoArchivo");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TbTipoArchivoPorSiniestro>(entity =>
            {
                entity.ToTable("tb_TipoArchivoPorSiniestro");

                entity.HasOne(d => d.IdTipoArchivoNavigation)
                    .WithMany(p => p.TbTipoArchivoPorSiniestro)
                    .HasForeignKey(d => d.IdTipoArchivo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tb_TipoArchivoPorSiniestro_tb_TipoArchivo");

                entity.HasOne(d => d.IdTipoSiniestroNavigation)
                    .WithMany(p => p.TbTipoArchivoPorSiniestro)
                    .HasForeignKey(d => d.IdTipoSiniestro)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tb_TipoArchivoPorSiniestro_tb_TipoSiniestro");
            });

            modelBuilder.Entity<TbTipoCarga>(entity =>
            {
                entity.HasKey(e => e.IdTipoCarga);

                entity.ToTable("tb_TipoCarga");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TbTipoEquipo>(entity =>
            {
                entity.HasKey(e => e.IdTipoEquipo)
                    .HasName("PK_TipoEquipo");

                entity.ToTable("tb_TipoEquipo");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TbTipoLesion>(entity =>
            {
                entity.HasKey(e => e.IdTipoLesion)
                    .HasName("PK_TipoLesion");

                entity.ToTable("tb_TipoLesion");

                entity.Property(e => e.Descripcion).HasMaxLength(50);
            });

            modelBuilder.Entity<TbTipoServicio>(entity =>
            {
                entity.HasKey(e => e.IdTipoServicio)
                    .HasName("PK_TipoServicio");

                entity.ToTable("tb_TipoServicio");

                entity.Property(e => e.Descripcion).HasMaxLength(10);
            });

            modelBuilder.Entity<TbTipoSiniestro>(entity =>
            {
                entity.HasKey(e => e.IdTipoSiniestro);

                entity.ToTable("tb_TipoSiniestro");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TbTipoTransporte>(entity =>
            {
                entity.HasKey(e => e.IdTipoTransporte);

                entity.ToTable("tb_TipoTransporte");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TbTipoTransportista>(entity =>
            {
                entity.HasKey(e => e.IdTipoTransportista);

                entity.ToTable("tb_TipoTransportista");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TbTransportista>(entity =>
            {
                entity.HasKey(e => e.IdTransportista);

                entity.ToTable("tb_Transportista");

                entity.Property(e => e.IdTransportista).ValueGeneratedNever();

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.MontoRecuperar).HasColumnType("decimal(10, 2)");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.Property(e => e.Code)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ExpireCode).HasColumnType("datetime");

                entity.Property(e => e.FirstName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SaltCode)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.User)
                    .HasForeignKey(d => d.RoleId);
            });

            modelBuilder.Entity<VwActivos>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_Activos");

                entity.Property(e => e.EstatusActivo).HasMaxLength(50);

                entity.Property(e => e.Fase).HasMaxLength(50);

                entity.Property(e => e.Fecha).HasColumnType("datetime");

                entity.Property(e => e.FechaAlta).HasColumnType("date");

                entity.Property(e => e.FechaBaja).HasColumnType("date");

                entity.Property(e => e.GrupoTrabajo).HasMaxLength(50);

                entity.Property(e => e.IdEquipo).HasMaxLength(50);

                entity.Property(e => e.IdTipoEquipo).HasMaxLength(50);

                entity.Property(e => e.Importe).HasColumnType("decimal(10, 0)");

                entity.Property(e => e.Marca).HasMaxLength(50);

                entity.Property(e => e.Modelo)
                    .HasMaxLength(10)
                    .IsFixedLength();

                entity.Property(e => e.Moneda).HasMaxLength(50);

                entity.Property(e => e.NoEconomico).HasMaxLength(10);

                entity.Property(e => e.NoMotor).HasMaxLength(17);

                entity.Property(e => e.Propietario).HasMaxLength(50);

                entity.Property(e => e.Serie).HasMaxLength(20);

                entity.Property(e => e.Sucursal).HasMaxLength(50);
            });

            modelBuilder.Entity<VwConfiguracionAviso>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_ConfiguracionAviso");

                entity.Property(e => e.Estatus).HasMaxLength(50);

                entity.Property(e => e.EstatusSig).HasMaxLength(50);

                entity.Property(e => e.Rol)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VwEmailResponsableAdministrativo>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_EmailResponsableAdministrativo");

                entity.Property(e => e.UserName)
                    .HasMaxLength(8000)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VwEmailResponsableCalidad>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_EmailResponsableCalidad");

                entity.Property(e => e.UserName)
                    .HasMaxLength(8000)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VwEmailResponsableCoorLogistica>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_EmailResponsableCoorLogistica");

                entity.Property(e => e.UserName)
                    .HasMaxLength(8000)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VwEmailResponsableCoorPci>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_EmailResponsableCoorPCI");

                entity.Property(e => e.UserName)
                    .HasMaxLength(8000)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VwEmailResponsableCoorRiesgos>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_EmailResponsableCoorRiesgos");

                entity.Property(e => e.UserName)
                    .HasMaxLength(8000)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VwEmailResponsableCoorTrafico>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_EmailResponsableCoorTrafico");

                entity.Property(e => e.UserName)
                    .HasMaxLength(8000)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VwEmailResponsableFinanzas>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_EmailResponsableFinanzas");

                entity.Property(e => e.UserName)
                    .HasMaxLength(8000)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VwEmailResponsableJefeAlmacen>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_EmailResponsableJefeAlmacen");

                entity.Property(e => e.UserName)
                    .HasMaxLength(8000)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VwEmailResponsablePlanta>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_EmailResponsablePlanta");

                entity.Property(e => e.UserName)
                    .HasMaxLength(8000)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VwEmailResponsableServicioCliente>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_EmailResponsableServicioCliente");

                entity.Property(e => e.UserName)
                    .HasMaxLength(8000)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VwEmailResponsableTransportista>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_EmailResponsableTransportista");

                entity.Property(e => e.UserName)
                    .HasMaxLength(8000)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VwOperadorCertificado>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_OperadorCertificado");

                entity.Property(e => e.Calificacion).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.FechaFin).HasColumnType("datetime");

                entity.Property(e => e.FechaVigencia).HasColumnType("datetime");

                entity.Property(e => e.VigenciaCertificacion)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VwOperadorLicencia>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_OperadorLicencia");

                entity.Property(e => e.AfiliacionImss)
                    .IsRequired()
                    .HasColumnName("AfiliacionIMSS")
                    .HasMaxLength(50);

                entity.Property(e => e.EstatusOperador)
                    .IsRequired()
                    .HasMaxLength(9)
                    .IsUnicode(false);

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(152);

                entity.Property(e => e.NumeroLicencia)
                    .IsRequired()
                    .HasMaxLength(10);

                entity.Property(e => e.Tipo)
                    .IsRequired()
                    .HasMaxLength(5);

                entity.Property(e => e.TransportistaDescripcion)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Vigencia)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.VigenciaVencida)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VwPlantaUsuario>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_PlantaUsuario");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VwReporteBi>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_ReporteBI");

                entity.Property(e => e.FechaSiniestro).HasColumnType("datetime");

                entity.Property(e => e.Zamount).HasColumnName("ZAMOUNT");

                entity.Property(e => e.ZapCarri).HasColumnName("ZAP_CARRI");

                entity.Property(e => e.ZtmAnto)
                    .HasColumnName("ZTM_ANTO")
                    .HasMaxLength(4000);

                entity.Property(e => e.ZtmClapi)
                    .HasColumnName("ZTM_CLAPI")
                    .HasMaxLength(50);

                entity.Property(e => e.ZtmClasi)
                    .HasColumnName("ZTM_CLASI")
                    .HasMaxLength(19)
                    .IsUnicode(false);

                entity.Property(e => e.ZtmCltrp)
                    .HasColumnName("ZTM_CLTRP")
                    .HasMaxLength(4000);

                entity.Property(e => e.ZtmCount).HasColumnName("ZTM_COUNT");

                entity.Property(e => e.ZtmDanad)
                    .IsRequired()
                    .HasColumnName("ZTM_DANAD")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.ZtmEdop)
                    .HasColumnName("ZTM_EDOP")
                    .HasMaxLength(4000);

                entity.Property(e => e.ZtmLesio)
                    .IsRequired()
                    .HasColumnName("ZTM_LESIO")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.ZtmMeta).HasColumnName("ZTM_META");

                entity.Property(e => e.ZtmMonn)
                    .HasColumnName("ZTM_MONN")
                    .HasColumnType("decimal(22, 4)");

                entity.Property(e => e.ZtmMonr)
                    .HasColumnName("ZTM_MONR")
                    .HasColumnType("decimal(21, 4)");

                entity.Property(e => e.ZtmMonrt)
                    .HasColumnName("ZTM_MONRT")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.ZtmMontr)
                    .HasColumnName("ZTM_MONTR")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.ZtmMrecs)
                    .HasColumnName("ZTM_MRECS")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.ZtmNopr)
                    .HasColumnName("ZTM_NOPR")
                    .HasColumnType("decimal(17, 2)");

                entity.Property(e => e.ZtmPenda)
                    .HasColumnName("ZTM_PENDA")
                    .HasColumnType("decimal(17, 2)");

                entity.Property(e => e.ZtmPendd)
                    .HasColumnName("ZTM_PENDD")
                    .HasColumnType("decimal(17, 2)");

                entity.Property(e => e.ZtmPendr)
                    .HasColumnName("ZTM_PENDR")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.ZtmProd)
                    .IsRequired()
                    .HasColumnName("ZTM_PROD")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.ZtmResp)
                    .IsRequired()
                    .HasColumnName("ZTM_RESP")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.ZtmRestr)
                    .IsRequired()
                    .HasColumnName("ZTM_RESTR")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.ZtmSinen).HasColumnName("ZTM_SINEN");

                entity.Property(e => e.ZtmSiner).HasColumnName("ZTM_SINER");

                entity.Property(e => e.ZtmSinie).HasColumnName("ZTM_SINIE");

                entity.Property(e => e.ZtmSinre).HasColumnName("ZTM_SINRE");

                entity.Property(e => e.ZtmSpgt).HasColumnName("ZTM_SPGT");

                entity.Property(e => e.ZtmStatr)
                    .HasColumnName("ZTM_STATR")
                    .HasMaxLength(50);

                entity.Property(e => e.ZtmStats)
                    .IsRequired()
                    .HasColumnName("ZTM_STATS")
                    .HasMaxLength(14)
                    .IsUnicode(false);

                entity.Property(e => e.ZtmTrecs)
                    .HasColumnName("ZTM_TRECS")
                    .HasColumnType("decimal(17, 2)");

                entity.Property(e => e.ZtmTsini)
                    .HasColumnName("ZTM_TSINI")
                    .HasMaxLength(50);

                entity.Property(e => e.ZtmUser)
                    .HasColumnName("ZTM_USER")
                    .HasMaxLength(50);

                entity.Property(e => e.ZtmValua).HasColumnName("ZTM_VALUA");

                entity.Property(e => e.ZtmdMonr)
                    .HasColumnName("ZTMD_MONR")
                    .HasColumnType("decimal(21, 4)");

                entity.Property(e => e.ZtmdPlan)
                    .HasColumnName("ZTMD_PLAN")
                    .HasMaxLength(4000);

                entity.Property(e => e.Ztmmodser)
                    .HasColumnName("ZTMMODSER")
                    .HasMaxLength(10);

                entity.Property(e => e.Ztmnacex)
                    .HasColumnName("ZTMNACEX")
                    .HasMaxLength(4000);

                entity.Property(e => e.Ztmtcarga)
                    .HasColumnName("ZTMTCARGA")
                    .HasMaxLength(4000);

                entity.Property(e => e._0amount)
                    .HasColumnName("0AMOUNT")
                    .HasColumnType("decimal(18, 2)");

                entity.Property(e => e._0apoLocfr)
                    .HasColumnName("0APO_LOCFR")
                    .HasMaxLength(4000);

                entity.Property(e => e._0calday)
                    .HasColumnName("0CALDAY")
                    .HasMaxLength(4000);

                entity.Property(e => e._0calmonth2).HasColumnName("0CALMONTH2");

                entity.Property(e => e._0currency)
                    .IsRequired()
                    .HasColumnName("0CURRENCY")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e._0region)
                    .HasColumnName("0REGION")
                    .HasMaxLength(3)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VwReporteCausaRaiz>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_ReporteCausaRaiz");

                entity.Property(e => e.DescCausaRaiz).HasMaxLength(4000);

                entity.Property(e => e.DescEstatusSiniestro).HasMaxLength(4000);

                entity.Property(e => e.DescPlantaResponsble).HasMaxLength(50);

                entity.Property(e => e.DescTipoSiniestro).HasMaxLength(50);

                entity.Property(e => e.DescTipoTransporte).HasMaxLength(50);

                entity.Property(e => e.DescTransportista).HasMaxLength(50);

                entity.Property(e => e.FechaSiniestro).HasColumnType("datetime");
            });

            modelBuilder.Entity<VwReporteEstatus>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_ReporteEstatus");

                entity.Property(e => e.DescEstatusSiniestro).HasMaxLength(4000);

                entity.Property(e => e.DescPlantaResponsble).HasMaxLength(50);

                entity.Property(e => e.DescTipoSiniestro).HasMaxLength(50);

                entity.Property(e => e.DescTipoTransporte).HasMaxLength(50);

                entity.Property(e => e.DescTransportista).HasMaxLength(50);

                entity.Property(e => e.FechaSiniestro).HasColumnType("datetime");
            });

            modelBuilder.Entity<VwReporteRecuperacionSin>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_ReporteRecuperacionSin");

                entity.Property(e => e.DescEstatusSiniestro).HasMaxLength(4000);

                entity.Property(e => e.DescPlantaResponsble).HasMaxLength(50);

                entity.Property(e => e.DescTipoSiniestro).HasMaxLength(50);

                entity.Property(e => e.DescTipoTransporte).HasMaxLength(50);

                entity.Property(e => e.DescTransportista).HasMaxLength(50);

                entity.Property(e => e.FechaSiniestro).HasColumnType("datetime");
            });

            modelBuilder.Entity<VwReporteRecuperacionSiniestros>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_ReporteRecuperacionSiniestros");

                entity.Property(e => e.FechaSiniestro).HasColumnType("datetime");

                entity.Property(e => e.Tipo)
                    .IsRequired()
                    .HasMaxLength(11)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VwReporteTiempoAtencion>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_ReporteTiempoAtencion");

                entity.Property(e => e.DescEstatus).HasMaxLength(4000);

                entity.Property(e => e.FechaActualizacion).HasColumnType("datetime");

                entity.Property(e => e.FechaEstatusAnterior).HasColumnType("datetime");

                entity.Property(e => e.FechaSiniestro).HasColumnType("datetime");
            });

            modelBuilder.Entity<VwResumenActivos>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_ResumenActivos");

                entity.Property(e => e.CajaSeca).HasColumnName("Caja Seca");

                entity.Property(e => e.RemolqueLateral).HasColumnName("Remolque Lateral");

                entity.Property(e => e.Sucursal).HasMaxLength(50);
            });

            modelBuilder.Entity<VwSiniestro>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_Siniestro");

                entity.Property(e => e.AntiguedadOperador).HasMaxLength(50);

                entity.Property(e => e.CostoTransferencia).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.CostoVenta).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.DescTipoTranportista)
                    .HasColumnName("descTipoTranportista")
                    .HasMaxLength(50);

                entity.Property(e => e.Destino).HasMaxLength(50);

                entity.Property(e => e.EstadoOcurrioSiniestro)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.EstatusDeAseguradora)
                    .IsRequired()
                    .HasMaxLength(14)
                    .IsUnicode(false);

                entity.Property(e => e.EstatusProducto).HasMaxLength(50);

                entity.Property(e => e.EstatusSiniestroDescripcion)
                    .HasColumnName("EstatusSiniestro_Descripcion")
                    .HasMaxLength(50);

                entity.Property(e => e.FechaNotaCredito).HasColumnType("datetime");

                entity.Property(e => e.FechaSiniestro).HasColumnType("datetime");

                entity.Property(e => e.Fo)
                    .HasColumnName("FO")
                    .HasColumnType("decimal(16, 0)");

                entity.Property(e => e.IdFatalidad).HasMaxLength(50);

                entity.Property(e => e.IdSiniestroText)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.IdTipoServicio).HasMaxLength(10);

                entity.Property(e => e.LineaSubcontratada3Pl)
                    .HasColumnName("LineaSubcontratada3PL")
                    .HasMaxLength(50);

                entity.Property(e => e.LineaTransportista).HasMaxLength(50);

                entity.Property(e => e.MontoEnRecuperacion).HasColumnType("decimal(21, 4)");

                entity.Property(e => e.MontoNegociado).HasColumnType("decimal(22, 4)");

                entity.Property(e => e.MontoNotaCredito).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.MontoPendienteRecuperarTrans).HasColumnType("decimal(21, 4)");

                entity.Property(e => e.MontoRecuperadoTrans).HasColumnType("decimal(21, 4)");

                entity.Property(e => e.MontoRecuperarAse).HasColumnType("decimal(22, 4)");

                entity.Property(e => e.NoNotaCredito).HasMaxLength(50);

                entity.Property(e => e.NoProcede).HasColumnType("decimal(22, 4)");

                entity.Property(e => e.NombreOperador).HasMaxLength(50);

                entity.Property(e => e.NumUnidad).HasMaxLength(20);

                entity.Property(e => e.Origen).HasMaxLength(50);

                entity.Property(e => e.Pedido1)
                    .HasMaxLength(16)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Pedido2)
                    .HasMaxLength(16)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.PendAnalisisAse).HasColumnType("decimal(22, 4)");

                entity.Property(e => e.PendienteDoc).HasColumnType("decimal(22, 4)");

                entity.Property(e => e.Piramide).HasMaxLength(50);

                entity.Property(e => e.PiramideDesc)
                    .HasMaxLength(19)
                    .IsUnicode(false);

                entity.Property(e => e.PlantaResponsable).HasMaxLength(50);

                entity.Property(e => e.ProductoDrp).HasColumnName("ProductoDRP");

                entity.Property(e => e.PuntoSiniestro).HasMaxLength(100);

                entity.Property(e => e.Ruta).HasMaxLength(103);

                entity.Property(e => e.TipoCambio).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.TipoCarga).HasMaxLength(50);

                entity.Property(e => e.TipoLesion).HasMaxLength(50);

                entity.Property(e => e.TipoSiniestro).HasMaxLength(50);

                entity.Property(e => e.TipoTransporte).HasMaxLength(50);

                entity.Property(e => e.TotalRecuperarAse).HasColumnType("decimal(22, 4)");
            });

            modelBuilder.Entity<VwSiniestroArchivo>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_SiniestroArchivo");

                entity.Property(e => e.Descripcion).HasMaxLength(500);

                entity.Property(e => e.Eliminar)
                    .IsRequired()
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.Fecha).HasColumnType("datetime");

                entity.Property(e => e.KeyBlob).HasMaxLength(50);

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.RepositorioAzure).HasMaxLength(500);

                entity.Property(e => e.TipoArchivoDesc)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<VwSiniestroAseguradora>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_SiniestroAseguradora");

                entity.Property(e => e.CostoTransferencia).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.CostoVenta).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.DescEstatusAseguradora).HasMaxLength(50);

                entity.Property(e => e.DescPlantaResponsable).HasMaxLength(50);

                entity.Property(e => e.DescTipoSiniestro).HasMaxLength(50);

                entity.Property(e => e.FechaSiniestro).HasColumnType("datetime");

                entity.Property(e => e.IdSiniestro)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.MontoPrecioConsumidor).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.Pedido1)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Pedido2)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.PrecioConsumidorUsd)
                    .HasColumnName("PrecioConsumidorUSD")
                    .HasColumnType("decimal(10, 2)");
            });

            modelBuilder.Entity<VwTipoArchivoAseguradora>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_TipoArchivoAseguradora");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.IdTipoArchivo).ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<VwUsuario>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_Usuario");

                entity.Property(e => e.DescPlanta).HasMaxLength(50);

                entity.Property(e => e.DescRol)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DescTransportista).HasMaxLength(50);

                entity.Property(e => e.FirstName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SaltCode)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
