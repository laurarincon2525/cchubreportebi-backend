﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class VwOperadorLicencia
    {
        public int IdTransportista { get; set; }
        public string Nombre { get; set; }
        public bool Estatus { get; set; }
        public string EstatusOperador { get; set; }
        public string NumeroLicencia { get; set; }
        public string Tipo { get; set; }
        public string Vigencia { get; set; }
        public string VigenciaVencida { get; set; }
        public int IdOperador { get; set; }
        public string AfiliacionImss { get; set; }
        public string TransportistaDescripcion { get; set; }
    }
}
