﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class VwEmailResponsableTransportista
    {
        public int? IdTransportista { get; set; }
        public string UserName { get; set; }
    }
}
