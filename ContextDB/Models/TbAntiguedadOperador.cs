﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class TbAntiguedadOperador
    {
        public int IdAntiguedadOperador { get; set; }
        public string Descripcion { get; set; }
    }
}
