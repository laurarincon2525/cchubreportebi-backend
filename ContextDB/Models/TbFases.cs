﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class TbFases
    {
        public int IdFases { get; set; }
        public string Descripcion { get; set; }
    }
}
