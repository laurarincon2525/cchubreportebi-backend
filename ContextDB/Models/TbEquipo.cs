﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class TbEquipo
    {
        public int IdEquipo { get; set; }
        public string Descripcion { get; set; }
    }
}
