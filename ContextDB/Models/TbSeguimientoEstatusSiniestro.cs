﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class TbSeguimientoEstatusSiniestro
    {
        public int IdSeguimientoEstatusSiniestro { get; set; }
        public int? IdSiniestro { get; set; }
        public int? EstatusAnterior { get; set; }
        public int? EstatusActual { get; set; }
        public int? IdUsuario { get; set; }
        public DateTime? FechaActualizacion { get; set; }
        public DateTime? FechaEstatusAnterior { get; set; }
    }
}
