﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class TbTipoLesion
    {
        public int IdTipoLesion { get; set; }
        public string Descripcion { get; set; }
    }
}
