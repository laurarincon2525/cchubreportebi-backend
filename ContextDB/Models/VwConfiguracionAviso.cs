﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class VwConfiguracionAviso
    {
        public int IdConfiguracionAviso { get; set; }
        public int? IdEstatus { get; set; }
        public string Estatus { get; set; }
        public int? IdEstatusSiguiente { get; set; }
        public string EstatusSig { get; set; }
        public int? TiempoAviso { get; set; }
        public int? IdCorreo { get; set; }
        public string Rol { get; set; }
    }
}
