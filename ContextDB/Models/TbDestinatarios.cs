﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class TbDestinatarios
    {
        public int IdDestinatario { get; set; }
        public int IdRol { get; set; }
    }
}
