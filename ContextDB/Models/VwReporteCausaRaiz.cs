﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class VwReporteCausaRaiz
    {
        public int IdEstatusSiniestro { get; set; }
        public int Expr1 { get; set; }
        public string DescCausaRaiz { get; set; }
        public string DescEstatusSiniestro { get; set; }
        public int? IdPlantaResponsable { get; set; }
        public string DescPlantaResponsble { get; set; }
        public int? IdTransportista { get; set; }
        public string DescTransportista { get; set; }
        public int? IdTipoSiniestro { get; set; }
        public string DescTipoSiniestro { get; set; }
        public int? IdTipoTransporte { get; set; }
        public string DescTipoTransporte { get; set; }
        public DateTime FechaSiniestro { get; set; }
    }
}
