﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class TbRespuestas
    {
        public int Id { get; set; }
        public int PreguntaId { get; set; }
        public string Descripcion { get; set; }
        public bool IsCorrecta { get; set; }
        public bool? Activa { get; set; }

        public virtual TbPreguntas Pregunta { get; set; }
    }
}
