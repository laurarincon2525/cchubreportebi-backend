﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class TbTipoSiniestro
    {
        public TbTipoSiniestro()
        {
            TbTipoArchivoPorSiniestro = new HashSet<TbTipoArchivoPorSiniestro>();
        }

        public int IdTipoSiniestro { get; set; }
        public string Descripcion { get; set; }

        public virtual ICollection<TbTipoArchivoPorSiniestro> TbTipoArchivoPorSiniestro { get; set; }
    }
}
