﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class TbLogAcciones
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Accion { get; set; }
        public int? IdSiniestro { get; set; }
        public DateTime Fecha { get; set; }
    }
}
