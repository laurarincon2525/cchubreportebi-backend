﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class TbConfiguraciones
    {
        public int IdConfiguraciones { get; set; }
        public string Configuracion { get; set; }
        public string Valor { get; set; }
    }
}
