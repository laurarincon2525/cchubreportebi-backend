﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class VwSiniestroAseguradora
    {
        public string IdSiniestro { get; set; }
        public int? IdSiniestroGlobal { get; set; }
        public DateTime FechaSiniestro { get; set; }
        public int? IdPlantaResponsable { get; set; }
        public string DescPlantaResponsable { get; set; }
        public decimal? MontoPrecioConsumidor { get; set; }
        public int? IdTipoSiniestro { get; set; }
        public string DescTipoSiniestro { get; set; }
        public int? IdEstatusAseguradora { get; set; }
        public string DescEstatusAseguradora { get; set; }
        public string Pedido1 { get; set; }
        public string Pedido2 { get; set; }
        public decimal? CostoTransferencia { get; set; }
        public decimal? CostoVenta { get; set; }
        public decimal? PrecioConsumidorUsd { get; set; }
        public string Valor { get; set; }
        public int? IdTransportista { get; set; }
    }
}
