﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class TbTransportista
    {
        public TbTransportista()
        {
            TbOperador = new HashSet<TbOperador>();
        }

        public int IdTransportista { get; set; }
        public string Descripcion { get; set; }
        public decimal? MontoRecuperar { get; set; }

        public virtual ICollection<TbOperador> TbOperador { get; set; }
    }
}
