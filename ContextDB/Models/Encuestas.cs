﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class Encuestas
    {
        public string Pregunta1 { get; set; }
        public bool? Pregunta2 { get; set; }
    }
}
