﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class VwSiniestroArchivo
    {
        public int IdSiniestro { get; set; }
        public string Nombre { get; set; }
        public int IdTipoArchivo { get; set; }
        public string TipoArchivoDesc { get; set; }
        public DateTime Fecha { get; set; }
        public string RepositorioAzure { get; set; }
        public string Descripcion { get; set; }
        public string Eliminar { get; set; }
        public int IdArchivo { get; set; }
        public string KeyBlob { get; set; }
    }
}
