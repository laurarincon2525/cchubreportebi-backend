﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class TbNotifications
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public string TriggerAt { get; set; }
        public string Severity { get; set; }
        public string UserId { get; set; }
        public bool? Active { get; set; }
    }
}
