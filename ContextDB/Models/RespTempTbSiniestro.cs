﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class RespTempTbSiniestro
    {
        public int IdSiniestro { get; set; }
        public DateTime FechaSiniestro { get; set; }
        public int IdEstatusSiniestro { get; set; }
        public int? IdPlantaResponsable { get; set; }
        public int? IdOrigen { get; set; }
        public string Destino { get; set; }
        public int? IdEstadoSiniestro { get; set; }
        public int? IdTransportista { get; set; }
        public int? IdTipoTransporte { get; set; }
        public int? IdTipoCarga { get; set; }
        public decimal? Fo { get; set; }
        public decimal? Pedido1 { get; set; }
        public decimal? Pedido2 { get; set; }
        public int? IdTipoSiniestro { get; set; }
        public int? IdEstatusProducto { get; set; }
        public int? IdPiramideSiniestro { get; set; }
        public int? IdDireccion { get; set; }
        public int? DocAnexoA { get; set; }
        public string LineaSubcontratada3Pl { get; set; }
        public int? IdTipoServicio { get; set; }
        public string NombreOperador { get; set; }
        public int? IdAntiguedadOperador { get; set; }
        public string NumUnidad { get; set; }
        public int? IdEdadOperador { get; set; }
        public bool? Lesionado { get; set; }
        public int? IdTipoLesion { get; set; }
        public bool? Incapacidad { get; set; }
        public int? DiasIncapacidad { get; set; }
        public bool? Alcohol { get; set; }
        public bool? Drogas { get; set; }
        public bool? Fatalidad { get; set; }
        public int? IdFatalidad { get; set; }
        public string Comentario { get; set; }
        public bool? NoAplicaPago { get; set; }
        public string NoNotaCredito { get; set; }
        public decimal? MontoNotaCredito { get; set; }
        public DateTime? FechaNotaCredito { get; set; }
        public decimal? CostoTransferencia { get; set; }
        public decimal? CostoVenta { get; set; }
        public decimal? TipoCambio { get; set; }
        public bool? ProductoDrp { get; set; }
        public bool? ProductoRecuperable { get; set; }
        public int? EdadOperador { get; set; }
        public string Origen { get; set; }
        public DateTime? FechaCreacion { get; set; }
        public DateTime? FechaModificacion { get; set; }
        public int? IdSiniestroGlobal { get; set; }
        public decimal? MontoPrecioConsumidor { get; set; }
        public int? IdEstatusAseguradora { get; set; }
        public int? IdCausaRaiz { get; set; }
        public string PuntoSiniestro { get; set; }
        public bool? RespTransportista { get; set; }
        public decimal? MontoRecuperadoTrans { get; set; }
        public string UbicacionT { get; set; }
        public int? IdTipoTransportista { get; set; }
        public string NumTracto { get; set; }
        public bool? TractoDanando { get; set; }
        public string NumRemolque1 { get; set; }
        public bool? Remolque1Danado { get; set; }
        public string NumRemolque2 { get; set; }
        public bool? Remolque2Danado { get; set; }
        public string NumDolly { get; set; }
        public bool? DollyDanado { get; set; }
        public string Username { get; set; }
    }
}
