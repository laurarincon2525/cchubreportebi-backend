﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class TbPropietario
    {
        public int IdPropietario { get; set; }
        public string Descripcion { get; set; }
    }
}
