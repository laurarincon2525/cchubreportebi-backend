﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class TbRemplazos
    {
        public int IdRemplazos { get; set; }
        public string Descripcion { get; set; }
    }
}
