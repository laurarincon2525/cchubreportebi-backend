﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class Customers
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public string Address { get; set; }
        public decimal? Salary { get; set; }
    }
}
