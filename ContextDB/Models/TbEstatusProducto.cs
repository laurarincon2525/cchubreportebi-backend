﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class TbEstatusProducto
    {
        public int IdEstatusProducto { get; set; }
        public string Descripcion { get; set; }
    }
}
