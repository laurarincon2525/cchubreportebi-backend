﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class TbTipoArchivo
    {
        public TbTipoArchivo()
        {
            TbTipoArchivoPorSiniestro = new HashSet<TbTipoArchivoPorSiniestro>();
        }

        public int IdTipoArchivo { get; set; }
        public string Descripcion { get; set; }

        public virtual ICollection<TbTipoArchivoPorSiniestro> TbTipoArchivoPorSiniestro { get; set; }
    }
}
