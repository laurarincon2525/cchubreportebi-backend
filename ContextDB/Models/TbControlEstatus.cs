﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class TbControlEstatus
    {
        public int IdControlEstatus { get; set; }
        public int? IdSiniestro { get; set; }
        public int? IdEstatusAnterior { get; set; }
        public int? IdEstatusActual { get; set; }
        public DateTime? Fecha { get; set; }
        public int? IdUsuario { get; set; }
    }
}
