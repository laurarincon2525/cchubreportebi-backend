﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class VwResumenActivos
    {
        public string Sucursal { get; set; }
        public int? Fijas { get; set; }
        public int? Movedores { get; set; }
        public int? Local { get; set; }
        public int? Ottawas { get; set; }
        public int? Dolly { get; set; }
        public int? Carretera { get; set; }
        public int? Otay { get; set; }
        public int? CajaSeca { get; set; }
        public int? RemolqueLateral { get; set; }
    }
}
