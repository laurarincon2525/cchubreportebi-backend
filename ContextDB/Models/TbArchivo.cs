﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class TbArchivo
    {
        public TbArchivo()
        {
            TbSiniestroArchivo = new HashSet<TbSiniestroArchivo>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public DateTime Fecha { get; set; }
        public long IdUsuario { get; set; }
        public string RepositorioAzure { get; set; }
        public string KeyBlob { get; set; }

        public virtual User IdUsuarioNavigation { get; set; }
        public virtual ICollection<TbSiniestroArchivo> TbSiniestroArchivo { get; set; }
    }
}
