﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class TbFatalidad
    {
        public int IdFatalidad { get; set; }
        public string Descripcion { get; set; }
    }
}
