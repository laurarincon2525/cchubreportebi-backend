﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class TbGrupoTrabajo
    {
        public int IdGrupoTrabajo { get; set; }
        public string Descripcion { get; set; }
    }
}
