﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class TbPlanta
    {
        public int IdPlanta { get; set; }
        public string Descripcion { get; set; }
        public string ResponsableCet { get; set; }
    }
}
