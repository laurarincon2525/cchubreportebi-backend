﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class TbPiramideSiniestro
    {
        public int IdPiramideSiniestro { get; set; }
        public string Descripcion { get; set; }
    }
}
