﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class TbMarcaEquipo
    {
        public int IdMarcaEquipo { get; set; }
        public string Descripcion { get; set; }
    }
}
