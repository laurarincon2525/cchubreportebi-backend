﻿using System;
using System.Collections.Generic;

namespace ContextDB.Models
{
    public partial class TbConfiguracionAviso
    {
        public int IdConfiguracionAviso { get; set; }
        public int? IdEstatus { get; set; }
        public int? IdEstatusSiguiente { get; set; }
        public int? TiempoAviso { get; set; }
        public int? IdCorreo { get; set; }
    }
}
