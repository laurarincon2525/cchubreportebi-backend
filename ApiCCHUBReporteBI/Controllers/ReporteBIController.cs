﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ApiCCHUBReporteBI.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Http;
using ContextDB.Models;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace ApiCCHUBReporteBI.Controllers
{
    [Route("api/")]
    [ApiController]
    public class ReporteBIController : Controller
    {
        private IConfiguration _configuration;
        private heigismxcchubxpazcuasddatabase01Context _db;

        protected IConfiguration Configuration => _configuration;
        protected ContextDB.Models.heigismxcchubxpazcuasddatabase01Context Db => _db;
        public ReporteBIController(heigismxcchubxpazcuasddatabase01Context eFContext, IConfiguration configuration)
        {
            _db = eFContext;
            _configuration = configuration;
        }

        [HttpGet("siniestros")]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public ActionResult getSiniestros()
        {
            try
            {
                var query = Db.VwReporteBi.ToList().OrderByDescending(t=>t.ZtmSinie);

                return Ok(query);



            }
            catch (Exception ex)
            {
                // Logger.LogError(ex, "AuthController Register", DateTime.UtcNow);
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [ProducesResponseType(200)]
        [ProducesResponseType(401)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        [HttpPost("reportebi/excel")]
        public async Task<IActionResult> ExcelBi([FromBody] PostFechas data)
        {
            try
            {
                var query = Db.VwReporteBi.Where(t => t.FechaSiniestro >= data.FechaDesde && t.FechaSiniestro <= data.FechaHasta).ToList().OrderByDescending(t=>t.ZtmSinie);

                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                ExcelPackage excel = new ExcelPackage();


                excel.Workbook.Worksheets.Add("Sheet 1");
                ExcelWorksheet ew = excel.Workbook.Worksheets[0];


                ExcelRichText richtextTicket = ew.Cells[1, 1].RichText.Add("ZTM_SINIE");
                ExcelRichText richtextTicket1 = ew.Cells[1, 2].RichText.Add("0CALDAY");
                ExcelRichText richtextTicket2 = ew.Cells[1, 3].RichText.Add("0CALMONTH2");
                ExcelRichText richtextTicket3 = ew.Cells[1, 4].RichText.Add("ZTMD_PLAN");
                ExcelRichText richtextTicket4 = ew.Cells[1, 5].RichText.Add("ZTM_USER");
                ExcelRichText richtextTicket5 = ew.Cells[1, 6].RichText.Add("0APO_LOCFR");
                ExcelRichText richtextTicket6 = ew.Cells[1, 7].RichText.Add("0REGION");
                ExcelRichText richtextTicket7 = ew.Cells[1, 8].RichText.Add("ZTM_CLTRP");
                ExcelRichText richtextTicket8 = ew.Cells[1, 9].RichText.Add("ZAP_CARRI");
                ExcelRichText richtextTicket9 = ew.Cells[1, 10].RichText.Add("ZTMNACEX");
                ExcelRichText richtextTicket10 = ew.Cells[1, 11].RichText.Add("ZTMTCARGA");
                ExcelRichText richtextTicket11 = ew.Cells[1, 12].RichText.Add("ZTMMODSER");
                ExcelRichText richtextTicket12 = ew.Cells[1, 13].RichText.Add("ZTM_TSINI");
                ExcelRichText richtextTicket13 = ew.Cells[1, 14].RichText.Add("ZTM_DANAD");
                ExcelRichText richtextTicket14 = ew.Cells[1, 15].RichText.Add("ZTM_RESP");
                ExcelRichText richtextTicket15 = ew.Cells[1, 16].RichText.Add("ZTM_ANTO");
                ExcelRichText richtextTicket16 = ew.Cells[1, 17].RichText.Add("ZTM_EDOP");
                ExcelRichText richtextTicket17 = ew.Cells[1, 18].RichText.Add("ZTM_LESIO");
                ExcelRichText richtextTicket18 = ew.Cells[1, 19].RichText.Add("ZTM_CLASI");
                ExcelRichText richtextTicket19 = ew.Cells[1, 20].RichText.Add("ZTM_CLAPI");
                ExcelRichText richtextTicket20 = ew.Cells[1, 21].RichText.Add("0AMOUNT");
                ExcelRichText richtextTicket21 = ew.Cells[1, 22].RichText.Add("0CURRENCY");
                ExcelRichText richtextTicket22 = ew.Cells[1, 23].RichText.Add("ZTM_SINRE");
                ExcelRichText richtextTicket23 = ew.Cells[1, 24].RichText.Add("ZAMOUNT");
                ExcelRichText richtextTicket24 = ew.Cells[1, 25].RichText.Add("ZTMD_MONR");
                ExcelRichText richtextTicket25 = ew.Cells[1, 26].RichText.Add("ZTM_COUNT");
                ExcelRichText richtextTicket26 = ew.Cells[1, 27].RichText.Add("ZTM_PROD");
                ExcelRichText richtextTicket27 = ew.Cells[1, 28].RichText.Add("ZTM_RESTR");
                ExcelRichText richtextTicket28 = ew.Cells[1, 29].RichText.Add("ZTM_VALUA");
                ExcelRichText richtextTicket29 = ew.Cells[1, 30].RichText.Add("ZTM_MONRT");
                ExcelRichText richtextTicket30 = ew.Cells[1, 31].RichText.Add("ZTM_SINER");
                ExcelRichText richtextTicket31 = ew.Cells[1, 32].RichText.Add("ZTM_MONR");
                ExcelRichText richtextTicket32 = ew.Cells[1, 33].RichText.Add("ZTM_MONTR");
                ExcelRichText richtextTicket33 = ew.Cells[1, 34].RichText.Add("ZTM_MONN");
                ExcelRichText richtextTicket34 = ew.Cells[1, 35].RichText.Add("ZTM_SINEN");
                ExcelRichText richtextTicket35 = ew.Cells[1, 36].RichText.Add("ZTM_PENDR");
                ExcelRichText richtextTicket36 = ew.Cells[1, 37].RichText.Add("ZTM_META");
                ExcelRichText richtextTicket37 = ew.Cells[1, 38].RichText.Add("ZTM_SPGT");
                ExcelRichText richtextTicket38 = ew.Cells[1, 39].RichText.Add("ZTM_STATS");
                ExcelRichText richtextTicket39 = ew.Cells[1, 40].RichText.Add("ZTM_STATR");
                ExcelRichText richtextTicket40 = ew.Cells[1, 41].RichText.Add("ZTM_TRECS");
                ExcelRichText richtextTicket41 = ew.Cells[1, 42].RichText.Add("ZTM_MRECS");
                ExcelRichText richtextTicket42 = ew.Cells[1, 43].RichText.Add("ZTM_PENDD");
                ExcelRichText richtextTicket43 = ew.Cells[1, 44].RichText.Add("ZTM_PENDA");
                ExcelRichText richtextTicket44 = ew.Cells[1, 45].RichText.Add("ZTM_NOPR");
                //ExcelRichText richtextTicket45 = ew.Cells[1, 46].RichText.Add("FechaSiniestro");

                int recordIndex = 2;

                foreach (var item in query)
                {
                    ew.Cells[recordIndex, 1].Value = item.ZtmSinie;
                    ew.Cells[recordIndex, 2].Value = item._0calday;
                    ew.Cells[recordIndex, 3].Value = item._0calmonth2;
                    ew.Cells[recordIndex, 4].Value = item.ZtmdPlan;
                    ew.Cells[recordIndex, 5].Value = item.ZtmUser;
                    ew.Cells[recordIndex, 6].Value = item._0apoLocfr;
                    ew.Cells[recordIndex, 7].Value = item._0region;
                    ew.Cells[recordIndex, 8].Value = item.ZtmCltrp;
                    ew.Cells[recordIndex, 9].Value = item.ZapCarri;
                    ew.Cells[recordIndex, 10].Value = item.Ztmnacex;
                    ew.Cells[recordIndex,11].Value = item.Ztmtcarga;
                    ew.Cells[recordIndex, 12].Value = item.Ztmmodser;
                    ew.Cells[recordIndex, 13].Value = item.ZtmTsini;
                    ew.Cells[recordIndex, 14].Value = item.ZtmDanad;
                    ew.Cells[recordIndex, 15].Value = item.ZtmResp;
                    ew.Cells[recordIndex, 16].Value = item.ZtmAnto;
                    ew.Cells[recordIndex, 17].Value = item.ZtmEdop;
                    ew.Cells[recordIndex, 18].Value = item.ZtmLesio;
                    ew.Cells[recordIndex, 19].Value = item.ZtmClasi;
                    ew.Cells[recordIndex, 20].Value = item.ZtmClapi;
                    ew.Cells[recordIndex, 21].Value = item._0amount;
                    ew.Cells[recordIndex, 22].Value = item._0currency;
                    ew.Cells[recordIndex, 23].Value = item.ZtmSinre;
                    ew.Cells[recordIndex, 24].Value = item.Zamount;
                    ew.Cells[recordIndex, 25].Value = item.ZtmdMonr;
                    ew.Cells[recordIndex, 26].Value = item.ZtmCount;
                    ew.Cells[recordIndex, 27].Value = item.ZtmProd;
                    ew.Cells[recordIndex, 28].Value = item.ZtmRestr;
                    ew.Cells[recordIndex, 29].Value = item.ZtmValua;
                    ew.Cells[recordIndex, 30].Value = item.ZtmMonrt;
                    ew.Cells[recordIndex, 31].Value = item.ZtmSiner;
                    ew.Cells[recordIndex, 32].Value = item.ZtmMonr;
                    ew.Cells[recordIndex, 33].Value = item.ZtmMontr;
                    ew.Cells[recordIndex, 34].Value = item.ZtmMonn;
                    ew.Cells[recordIndex, 35].Value = item.ZtmSinen;
                    ew.Cells[recordIndex, 36].Value = item.ZtmPendr;
                    ew.Cells[recordIndex, 37].Value = item.ZtmMeta;
                    ew.Cells[recordIndex, 38].Value = item.ZtmSpgt;
                    ew.Cells[recordIndex, 39].Value = item.ZtmStats;
                    ew.Cells[recordIndex, 40].Value = item.ZtmStatr;
                    ew.Cells[recordIndex, 41].Value = item.ZtmTrecs;
                    ew.Cells[recordIndex, 42].Value = item.ZtmMrecs;
                    ew.Cells[recordIndex, 43].Value = item.ZtmPendd;
                    ew.Cells[recordIndex, 44].Value = item.ZtmPenda;
                    ew.Cells[recordIndex, 45].Value = item.ZtmNopr.ToString();
                   // ew.Cells[recordIndex, 46].Value = item.FechaSiniestro.ToString();
                    recordIndex++;
                }


                var connectionString = Configuration["BlobStringConnection"];
                var storageAccount = CloudStorageAccount.Parse(connectionString);
                var client = storageAccount.CreateCloudBlobClient();
                var container = client.GetContainerReference("reportebi");
                await container.CreateIfNotExistsAsync();

                var guid = Guid.NewGuid();
                var newBlobName = $"{guid.ToString()}.xlsx";
                CloudBlockBlob blockBlob = container.GetBlockBlobReference(newBlobName);
                using (var stream = await blockBlob.OpenWriteAsync())
                {
                    excel.SaveAs(stream);
                }

                var dts = new
                {
                    url = blockBlob.Uri.ToString()
                };

                return Ok(dts);

            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex);
            }
        }


    }
}
